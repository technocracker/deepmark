<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324083412 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE profile ADD liveResourceOrigin_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE profile ADD CONSTRAINT FK_8157AA0F97EE03F1 FOREIGN KEY (liveResourceOrigin_id) REFERENCES live_resource_origin (id)');
        $this->addSql('CREATE INDEX IDX_8157AA0F97EE03F1 ON profile (liveResourceOrigin_id)');

    }

    public function down(Schema $schema) : void
    {

    }
}
