<?php

namespace LearnerBundle\Controller;

use ApiBundle\Controller\RestController;
use ApiBundle\Entity\Activities;
use ApiBundle\Entity\AssigmentResourceSystem;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\ModuleSession;
use ApiBundle\Entity\ModuleTrainerResponse;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ScormSynthesis;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Repository\BookingAgendaRepository;
use ApiBundle\Repository\LearnerInterventionRepository;
use ApiBundle\Repository\ProfileRepository;
use AppBundle\Entity\Person;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Traits\Controller\HasUsers;
use Traits\IsAvailable;
use Traits\IsScorm;
use Utils\Email;

/**
 * Class myCoursesApiController
 * @package LearnerBundle\Controller
 */
class myCoursesApiController extends RestController
{
    use Noticeable;
    use HelperTrait;
    use IsAvailable;
    use HasUsers;
    use IsScorm;

    const RESPONSE_OK = 'ok';

    /**
     * @return LearnerInterventionRepository
     */
    private function getLearnerInterventionRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention');
    }

    /**
     * @return ProfileRepository
     */
    private function getProfileRepository()
    {
        return $this->getDoctrine()->getRepository('ApiBundle:Profile');
    }

    /**
     * return the profile object for the current user.
     *
     * @return Profile|object
     */
    private function getCurrentUser()
    {
        $person     = $this->getUser();
        $profileRep = $this->getProfileRepository();

        return $profileRep->findOneBy(['person' => $person]);
    }

    /**
     * GET calendar
     * Display my bookings for an intervention.
     *
     * @Get("calendars/bookings/{interventionId}")
     *
     * @param int $interventionId
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     *
     * @throws Exception
     * @QueryParam(name="dateStart", nullable=false, description="Date Start")
     * @QueryParam(name="dateEnd", nullable=false, description="Date End")
     */
    public function getCalendarBookingsAction($interventionId, ParamFetcher $paramFetcher)
    {
        $now = new DateTime();
        if ($paramFetcher->get('dateStart')) {
            $dateStart = new DateTime($paramFetcher->get('dateStart'));
        } else {
            // first day of current month
            $dateStart = clone $now;
            $dateStart = $dateStart->modify('first day of this month');
        }
        if ($paramFetcher->get('dateEnd')) {
            $dateEnd = new DateTime($paramFetcher->get('dateEnd'));
        } else {
            // last day of month
            $dateEnd = clone $now;
            $dateEnd = $dateEnd->modify('last day of this month');
        }

        $em           = $this->getDoctrine()->getManager();
        $intervention = $em->getRepository('ApiBundle:Intervention')->find($interventionId);
        if (!$intervention) {
            return $this->errorHandler();
        }

        $array_bookings = [];
        /** @var BookingAgendaRepository $bookinAgendaRepository */
        $bookinAgendaRepository = $em->getRepository('ApiBundle:BookingAgenda');
        $bookings               = $bookinAgendaRepository->findMyBookingByIntervention($this->getCurrentUser(),
            $interventionId, $dateStart, $dateEnd);

        foreach ($bookings as $booking) {
            $module  = $booking->getModule();
            $end     = clone $booking->getBookingDate();
            $hour    = (int)$module->getDuration()->format('G') + (int)$booking->getBookingDate()->format('G');
            $minutes = (int)$module->getDuration()->format('i') + (int)$booking->getBookingDate()->format('i');
            $end->setTime($hour, $minutes, 0);

            $color = '#68d2f1';

            switch ($module->getStoredModule()->getAppId()) {
                case StoredModule::ONLINE:
                    $color = '#003B5C';
                    break;
                case StoredModule::VIRTUAL_CLASS:
                    $color = '#045f7f';
                    break;
                case StoredModule::ONLINE_WORKSHOP:
                    $color = '#68d2f1';
                    break;
                case StoredModule::PRESENTATION_ANIMATION:
                    $color = '#68d2f1';
                    break;
                default:
                    break;
            }

            array_push($array_bookings, array(
                'id'              => $booking->getModule()->getId(),
                'title'           => $booking->getBookingDate()->format('H:i') . ' - ' . $end->format('H:i') . ' ' . $booking->getModule()->getStoredModule()->getDesignation(),
                'start'           => $booking->getBookingDate(),
                'end'             => $end,
                'type'            => $booking->getModule()->getStoredModule()->getAppId(),
                'displayType'     => $booking->getModule()->getDesignation(),
                'duration'        => $booking->getModule()->getDuration()->format('G:i'),
//                'with' => $booking->getTrainer(),
//                'booking' => $booking,
                'backgroundColor' => $color,
                'className'       => ['event-booking'],
            ));
        }
        $view = View::create();

        $view->setData($array_bookings);

        return $this->handleView($view);
    }

    /**
     * GET calendar
     * Display trainers availabilities or all bookings including mine.
     *
     * @Get("calendars/online/{moduleId}")
     *
     * @param int $moduleId moduleId
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     *
     * @throws Exception
     * @QueryParam(name="dateStart", nullable=true, description="Date Start")
     * @QueryParam(name="dateEnd", nullable=true, description="Date End")
     * @QueryParam(name="trainer", nullable=true, description="Trainer Id")
     */
    public function getCalendarOnlineAction($moduleId, ParamFetcher $paramFetcher)
    {
        /** @var Person $user */
        $user = $this->getUser();
        if ($paramFetcher->get('dateStart')) {
            $dateStart = new DateTime($paramFetcher->get('dateStart') . ' 00:00:00', $user->getDateTimeZone());
        } else {
            $dateStart = new DateTime('first day of this month', $user->getDateTimeZone());
        }

        if ($paramFetcher->get('dateEnd')) {
            $dateEnd = new DateTime($paramFetcher->get('dateEnd') . ' 23:59:59', $user->getDateTimeZone());
        } else {
            $dateEnd = new DateTime('last day of this month', $user->getDateTimeZone());
        }

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->find($moduleId);

        if (!$module) {
            return $this->errorHandler();
        }

        $assignementResources = $this->getAssigmentResources($module, $paramFetcher->get('trainer'));

        $daysOpen = null;
        /** @var Intervention $intervention */
        $intervention = $module->getIntervention();
        if ($intervention->getOpenDays()) {
            $daysOpen = explode(',', $intervention->getOpenDays());
        }

        $excludeWeeks = $this->getDoctrine()->getRepository('ApiBundle:ExcludeWeeks')->findBy(['intervention' => $intervention]);
        $daySlots     = $this->getDoctrine()->getRepository('ApiBundle:DaySlots')->findBy(['intervention' => $intervention]);
        $days = $this->createDaysEntriesForCalendar($assignementResources, $dateStart, $dateEnd, $daysOpen?$daysOpen:[], $module,
            $excludeWeeks, $daySlots);
        $days = $this->filterDaysByBooking($days, $module);
        foreach ($days as $key => $day) {
            if (!empty($day['sessions'])) {
                $daySort = $day['sessions'];
                ksort($daySort);
                $days[$key]['sessions'] = $daySort;
            }

        }
        $view = View::create();

        $view->setData([
            'sessions' => array_values($days),
            'booked'   => $this->getBooked($module),
        ]);

        return $this->handleView($view);
    }

    private function filterDaysByBooking(array $days, $module): array
    {
        foreach ($days as $key => $day) {
            // issue-303: find the booking in the range time $day['start'] and $day['end']
            $bookings = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findBookingByModule($module->getId(), $day['start'], $day['end']);

            // issue-303: foreach the bookings to find the existing booking in the booking agenda.
            // If the booking is existing then system will don't allow booking by delete this session.
            foreach ($bookings as $booking) {
                $datetime                      = $booking->getBookingDate()->format('Y-m-d H:i');
                $days[$key]['sessionsRemoved'] = $days[$key]['sessionsRemoved'] + 1;
                // unset the the trainer if the booking date ($datetime) is existing in the $days array
                unset($days[$key]['sessions'][$datetime]['trainers'][$booking->getTrainer()->getId()]);
                if (array_key_exists($datetime, $days[$key]['sessions'])
                    && is_array($days[$key]['sessions'][$datetime]) && 0 == count($days[$key]['sessions'][$datetime])) {
                    unset($days[$key]['sessions'][$datetime]);
                }
            }

            foreach ($day['sessions'] as $k1 => $session) {
                if ($session['trainers']) {
                    foreach ($session['trainers'] as $k => $trainer) {
                        $end   = null;
                        $date  = explode(" ", $k1);
                        $start = \DateTime::createFromFormat('Y-m-d', $date[0]);
                        $time  = explode(":", $date[1]);
                        $start->setTime($time[0], $time[1], 0);

                        $end  = \DateTime::createFromFormat('Y-m-d', $date[0]);
                        $time = explode(":", $session['endDate']);
                        $end->setTime($time[0], $time[1], 0);

                        $trainer              = $this->getDoctrine()
                            ->getRepository('ApiBundle:Profile')
                            ->find($k);
                        $availableBook        = $this->isAvailableBookingByProfile($trainer, $module, $day['start'],
                            ['start' => $start, 'end' => $end]);
                        $availableBookLearner = $this->isAvailableBookingByProfile($this->getCurrentUser(), $module,
                            $day['start'], ['start' => $start, 'end' => $end]);
                        //$availableBook = !$this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                        //->isTrainerBooked($trainer, $start, $end);
                        if (!$availableBook || !$availableBookLearner) {
                            unset($days[$key]['sessions'][$k1]['trainers'][$k]);
                        }
                    }
                }
            }
        }

        return $days;
    }

    /*
     * Use for get working hours and lunch hour by profile
     */
    private function getWorkingHoursAndLunchTimeByProfile(
        Profile $profile,
        &$lunchStartTime,
        &$lunchEndTime,
        &$workingHoursByDay
    ) {
        $lunchStartTime = $profile->getLunchStartTime() != null ? $profile->getLunchStartTime()->format('H:i') : null;
        $lunchEndTime   = $profile->getLunchEndTime() != null ? $profile->getLunchEndTime()->format('H:i') : null;

        $workingHours = $this->getDoctrine()->getRepository('ApiBundle:WorkingHours')
            ->findBy(['profile' => $profile]);
        if ($workingHours) {
            foreach ($workingHours as $v) {
                if ($v != null && $v->getStart() != null) {
                    $workingHoursByDay[$v->getName()]['start'] = $v->getStart()->format('H:i');
                    $workingHoursByDay[$v->getName()]['end']   = $v->getEnd()->format('H:i');
                }
            }
        }
    }

    /**
     * @param array $assignementResources
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @param $daysOpen
     * @param Module $module
     *
     * @return array
     *
     * @throws Exception
     */
    private function createDaysEntriesForCalendar(
        array $assignementResources,
        DateTime $dateStart,
        DateTime $dateEnd,
        array $daysOpen,
        Module $module,
        $excludeWeeks,
        $dateSlots
    ) {
        $days = [];
        /** @var DateTime $duration */
        //$duration = $module->getDuration();

        //$duration->add(DateInterval::createFromDateString('15 minutes'));
        $interval           = DateInterval::createFromDateString('15 minutes');
        $profile            = $this->getCurrentUser();

        // set $tmpDaySlots setting with $dateSlots
        $tmpDaySlots = [];
        foreach ($dateSlots as $dateSlot) {
            $tmpDaySlots[$dateSlot->getDayId()] = [
                'startMorning'   => $dateSlot->getStartMorningTime(),
                'endMorning'     => $dateSlot->getEndMorningTime(),
                'startAfternoon' => $dateSlot->getStartAfternoonTime(),
                'endAfternoon'   => $dateSlot->getEndAfternoonTime(),
            ];
        }

        foreach ($assignementResources as $assignementResource) {
            $liveResource = $assignementResource->getLiveResource();
            if ($liveResource)
            {
                // find the AvailableCalendar between $dateStart and $dateEnd
                $availabilities = $this->getDoctrine()->getRepository('ApiBundle:AvailableCalendar')
                    ->findAvailabilityByLiveResource($liveResource, $dateStart, $dateEnd);

                foreach ($availabilities as $available) {
                    $startTime = $available->getBeginning();
                    $endTime = $available->getEnding();

                    $availableStartTime = $available->getBeginning()->format('H:i');
                    $availableEndTime = $available->getEnding()->format('H:i');
                    $dayOfWeek = $startTime->format('l');

                    // exclude date if this date in the exclude weeks range
                    $continue = false;
                    if ($excludeWeeks) {
                        foreach ($excludeWeeks as $excludeWeek) {
                            if ($excludeWeek->getStart() <= $startTime && $startTime < $excludeWeek->getEnd()) {
                                $continue = true;
                                continue;
                            }
                        }
                        if ($continue) {
                            continue;
                        }
                    }

                    // find the $lunchStartTime and $lunchEndTime of the leaner
                    $lunchStartTime     = null;
                    $lunchEndTime       = null;
                    $currentAvailabilityLearner = null;

                    if ($profile->getWorkingHours()) {
                        foreach ($profile->getWorkingHours() as $key => $availabilityLearner) {
                            if ($dayOfWeek == $key){
                                if ((!$availabilityLearner['startTime'] || !$availabilityLearner['endTime']) && ($availabilityLearner['startTimeAfternoon'] && $availabilityLearner['endTimeAfternoon'])) {
                                    $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTimeAfternoon'];
                                    $currentAvailabilityLearner['ending'] = $availabilityLearner['endTimeAfternoon'];
                                } else if (($availabilityLearner['startTime'] && $availabilityLearner['endTime']) && (!$availabilityLearner['startTimeAfternoon'] || !$availabilityLearner['endTimeAfternoon'])) {
                                    $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTime'];
                                    $currentAvailabilityLearner['ending'] = $availabilityLearner['endTime'];
                                } else if ($availabilityLearner['startTime'] && $availabilityLearner['endTime'] && $availabilityLearner['startTimeAfternoon'] && $availabilityLearner['endTimeAfternoon']) {
                                    $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTime'];
                                    $currentAvailabilityLearner['ending'] = $availabilityLearner['endTimeAfternoon'];
                                    $lunchStartTime = $availabilityLearner['endTime'];
                                    $lunchEndTime = $availabilityLearner['startTimeAfternoon'];
                                }
                            }
                        }
                    }

                    // find the $lunchStartTimeTrainer and $lunchEndTimeTrainer of the trainer
                    $lunchStartTimeTrainer = null;
                    $lunchEndTimeTrainer = null;
                    // get freezeTime to replace the lunch time for trainer
                    $freezeTimeTrainer = $available->getFreezeTime();
                    if ($freezeTimeTrainer) {
                        $freezeTimeArray = $freezeTimeTrainer != null ? explode("|", $freezeTimeTrainer) : null;
                        $lunchStartTimeTrainer = $freezeTimeArray != null ? $freezeTimeArray[0] : null;
                        $lunchEndTimeTrainer = $freezeTimeArray != null ? $freezeTimeArray[1] : null;
                    }

                    if ($lunchStartTime && $lunchEndTime) {
                        if ($lunchStartTimeTrainer && $lunchEndTimeTrainer) {
                            $lunchStartTimeTrainer = strtotime($lunchStartTime) < strtotime($lunchStartTimeTrainer) ? $lunchStartTime : $lunchStartTimeTrainer;
                            $lunchEndTimeTrainer = strtotime($lunchEndTime) > strtotime($lunchEndTimeTrainer) ? $lunchEndTime : $lunchEndTimeTrainer;
                        } else {
                            $lunchStartTimeTrainer = $lunchStartTime;
                            $lunchEndTimeTrainer = $lunchEndTime;
                        }
                    }

                    if ($lunchStartTimeTrainer && $lunchEndTimeTrainer) {
                        if ($currentAvailabilityLearner) {
                            if (!empty($currentAvailabilityLearner['beginning'])) {
                                $availableStartTime = strtotime($availableStartTime) < strtotime($currentAvailabilityLearner['beginning']) ? $currentAvailabilityLearner['beginning'] : $availableStartTime;
                            }
                            if (!empty($currentAvailabilityLearner['ending'])) {
                                $availableEndTime = strtotime($availableEndTime) > strtotime($currentAvailabilityLearner['ending']) ? $currentAvailabilityLearner['ending'] : $availableEndTime;
                            }
                        }

                        if ($tmpDaySlots) {
                            $dateWillGetSetting = $startTime->format('N');
                            if (array_key_exists($dateWillGetSetting,
                                    $tmpDaySlots) && !empty($tmpDaySlots[$dateWillGetSetting]['startMorning'])) {
                                $availableStartTime = strtotime($availableStartTime) < strtotime($tmpDaySlots[$dateWillGetSetting]['startMorning']->format('H:i:s')) ? $tmpDaySlots[$dateWillGetSetting]['startMorning']->format('H:i:s') : $availableStartTime;
                            }
                            if (array_key_exists($dateWillGetSetting,
                                    $tmpDaySlots) && !empty($tmpDaySlots[$dateWillGetSetting]['endAfternoon'])) {
                                $availableStartTime = strtotime($availableStartTime) > strtotime($tmpDaySlots[$dateWillGetSetting]['endAfternoon']->format('H:i:s')) ? $tmpDaySlots[$dateWillGetSetting]['endAfternoon']->format('H:i:s') : $availableStartTime;
                            }
                            if (array_key_exists($dateWillGetSetting,
                                    $tmpDaySlots) && !empty($tmpDaySlots[$dateWillGetSetting]['endMorning'])) {
                                $lunchStartTimeTrainer = strtotime($lunchStartTimeTrainer) < strtotime($tmpDaySlots[$dateWillGetSetting]['endMorning']->format('H:i:s')) ? $tmpDaySlots[$dateWillGetSetting]['endMorning']->format('H:i:s') : $lunchStartTimeTrainer;
                            }
                            if (array_key_exists($dateWillGetSetting,
                                    $tmpDaySlots) && !empty($tmpDaySlots[$dateWillGetSetting]['startAfternoon'])) {
                                $lunchEndTimeTrainer = strtotime($lunchEndTimeTrainer) > strtotime($tmpDaySlots[$dateWillGetSetting]['startAfternoon']->format('H:i:s')) ? $tmpDaySlots[$dateWillGetSetting]['startAfternoon']->format('H:i:s') : $lunchEndTimeTrainer;
                            }
                        }

                        if (strtotime($availableStartTime) >= strtotime($lunchEndTimeTrainer) || strtotime($availableEndTime) <= strtotime($lunchStartTimeTrainer)) {
                            $period = new DatePeriod(
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableStartTime),
                                $interval,
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime)
                            );

                            foreach ($period as $date) {
                                if (!$this->canBeAddedToDays($date, $daysOpen)) {
                                    continue;
                                }
                                $this->checkPeriodDate($date, $days, $available, $module, $liveResource,
                                    new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime));
                            }

                        } elseif (strtotime($availableStartTime) < strtotime($lunchStartTimeTrainer) && strtotime($availableEndTime) > strtotime($lunchEndTimeTrainer)) {
                            $period = new DatePeriod(
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableStartTime),
                                $interval,
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $lunchStartTimeTrainer)
                            );

                            foreach ($period as $date) {
                                if (!$this->canBeAddedToDays($date, $daysOpen)) {
                                    continue;
                                }
                                $this->checkPeriodDate($date, $days, $available, $module, $liveResource,
                                    new DateTime($available->getBeginning()->format("Y-m-d ") . $lunchStartTimeTrainer));
                            }

                            $period = new DatePeriod(
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $lunchEndTimeTrainer),
                                $interval,
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime)
                            );

                            foreach ($period as $date) {
                                if (!$this->canBeAddedToDays($date, $daysOpen)) {
                                    continue;
                                }
                                $this->checkPeriodDate($date, $days, $available, $module, $liveResource,
                                    new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime));
                            }


                        } elseif (strtotime($availableStartTime) >= strtotime($lunchStartTimeTrainer) && strtotime($availableStartTime) <= strtotime($lunchEndTimeTrainer) && strtotime($availableEndTime) > strtotime($lunchEndTimeTrainer)) {
                            $period = new DatePeriod(
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $lunchEndTimeTrainer),
                                $interval,
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime)
                            );

                            foreach ($period as $date) {
                                if (!$this->canBeAddedToDays($date, $daysOpen)) {
                                    continue;
                                }
                                $this->checkPeriodDate($date, $days, $available, $module, $liveResource,
                                    new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime));
                            }


                        } elseif (strtotime($availableEndTime) >= strtotime($lunchStartTimeTrainer) && strtotime($availableEndTime) <= strtotime($lunchEndTimeTrainer) && strtotime($availableStartTime) < strtotime($lunchStartTimeTrainer)) {
                            $period = new DatePeriod(
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableStartTime),
                                $interval,
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $lunchStartTimeTrainer)
                            );

                            foreach ($period as $date) {
                                if (!$this->canBeAddedToDays($date, $daysOpen)) {
                                    continue;
                                }
                                $this->checkPeriodDate($date, $days, $available, $module, $liveResource,
                                    new DateTime($available->getBeginning()->format("Y-m-d ") . $lunchStartTimeTrainer));
                            }

                        }

                    } else {
                        if ($currentAvailabilityLearner) {
                            if (!empty($currentAvailabilityLearner['beginning'])) {
                                $availableStartTime = strtotime($availableStartTime) < strtotime($currentAvailabilityLearner['beginning']) ? $currentAvailabilityLearner['beginning'] : $availableStartTime;
                            }
                            if (!empty($currentAvailabilityLearner['ending'])) {
                                $availableEndTime = strtotime($availableEndTime) > strtotime($currentAvailabilityLearner['ending']) ? $currentAvailabilityLearner['ending'] : $availableEndTime;
                            }
                        }

                        $period = new DatePeriod(
                            new DateTime($available->getBeginning()->format("Y-m-d ") . $availableStartTime),
                            $interval,
                            new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime)
                        );

                        foreach ($period as $date) {
                            if (!$this->canBeAddedToDays($date, $daysOpen)) {
                                continue;
                            }
                            $this->checkPeriodDate($date, $days, $available, $module, $liveResource,
                                new DateTime($available->getBeginning()->format("Y-m-d ") . $availableEndTime));
                        }

                    }
                }
            }
        }

        return $days;
    }

    private function checkPeriodDate($date, &$days, $available, $module, $liveResource, $endPeriod)
    {

        $day = $date->format('Y-m-d');
        if (!array_key_exists($day, $days)) {
            $days[$day] = array(
                'id'              => $available->getId(),
                'title'           => $module->getDesignation(),
                'start'           => $available->getBeginning(),
                'end'             => $available->getEnding(),
                'type'            => $module->getStoredModule()->getAppId(),
                'displayType'     => 'availability',
                'duration'        => $module->getDuration()->format('G:i'),
                'sessions'        => [],
                'sessionsNb'      => 0,
                'sessionsRemoved' => 0,
            );
        }

        $days[$day]['sessionsNb'] = $days[$day]['sessionsNb'] + 1;
        $datetimeFormat = 'Y-m-d H:i';
        $datetime    = $date->format($datetimeFormat);
        $durationEnd = clone $module->getDuration();
        //$durationEnd->sub(DateInterval::createFromDateString('15 minutes'));

        $hoursEnd   = intval($durationEnd->format('H'));
        $minutesEnd = intval($durationEnd->format('i'));

        $intervalEnd = new DateInterval('PT' . $hoursEnd . 'H' . $minutesEnd . 'M');
        $endDate     = clone $date->add($intervalEnd);

        $endTime       = $endDate->format('H:i');
        $endTimePeriod = $endPeriod->format('H:i');

        // only allow booking before 24h
        $nextDate = (new \DateTime())->modify('+24 hours');

        if ($endTime <= $endTimePeriod && $date >= $nextDate) {
            if (!array_key_exists($datetime, $days[$day]['sessions'])) {
                $days[$day]['sessions'][$datetime]                                     = [];
                $days[$day]['sessions'][$datetime]['endDate']                          = $endDate->format('H:i');
                $days[$day]['sessions'][$datetime]['trainers'][$liveResource->getId()] =
                    $liveResource->getFirstName() . ' ' . $liveResource->getLastName();
            } else {
                $days[$day]['sessions'][$datetime]['trainers'][$liveResource->getId()] =
                    $liveResource->getFirstName() . ' ' . $liveResource->getLastName();
            }
        }


    }

    private function canBeAddedToDays($date, $daysOpen = []): bool
    {
        // only allow booking before 24h
        $nextDate = (new \DateTime())->modify('+24 hours');
        /** @var DateTime $date */
        if ($date <= $nextDate) {
            return false;
        }

        if ($daysOpen) {
            $dayName = $date->format('w');
            if (!in_array($dayName, $daysOpen)) {
                return false;
            }
        }

        return true;
    }

    private function getBooked($module)
    {
        /** @var BookingAgenda $bookingModule */
        $bookingModule = $this->getDoctrine()
            ->getRepository('ApiBundle:BookingAgenda')
            ->findOneBy(array(
                'learner' => $this->getCurrentUser(),
                'module'  => $module,
            ));

        $booked = null;
        $gmtTimezone = new \DateTimeZone('Asia/Bangkok');
        $myDateTime = new \DateTime('2020-04-17 09:00', $gmtTimezone);
        if ($bookingModule) {
            $booked = array(
                'booking_date' => $bookingModule->getbookingDate(),
                'trainer'      => array(
                    'first_name' => $bookingModule->getTrainer()->getFirstName(),
                    'last_name'  => $bookingModule->getTrainer()->getLastName(),
                ), 'date_test' => $myDateTime
            );
        }

        return $booked;
    }

    /**
     * @param Module $module
     * @param string $trainerId
     *
     * @return object[]
     */
    private function getAssigmentResources(Module $module, string $trainerId = null)
    {
        $filters = ['module' => $module];
        if ($trainerId) {
            $filters['liveResource'] = $trainerId;
        }

        return $this->getDoctrine()
            ->getRepository('ApiBundle:AssigmentResourceSystem')
            ->findBy($filters);
    }

    /**
     * GET calendar
     * Display trainers availabilities or all bookings including mine.
     *
     * @Get("calendars/sessions/{moduleId}")
     *
     * @param int $moduleId moduleId
     *
     * @return Response
     *
     * @throws Exception
     * @QueryParam(name="dateStart", nullable=true, description="Date Start")
     * @QueryParam(name="dateEnd", nullable=true, description="Date End")
     */
    public function getCalendarSessionsAction($moduleId)
    {
        $now = new \DateTime('now');
        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->find($moduleId);
        if (!$module) {
            $this->errorHandler();
        }

        $profile = $this->getCurrentUser();
        $storedModule = $module->getStoredModule()->getAppId();
        $classRoomTraining = StoredModule::PRESENTATION_ANIMATION == $storedModule ? true : false;

        $sessionsDay = [];
        // get all moduleSession of the module
        $sessions = $module->getSessions();
        // check the config allow personal can book
        $allowPersonalBook = $module->getIsPersonalBooking() === 1 ? true : false;
        foreach ($sessions as $session) {
            $start = $session->getSession()->getSessionDate();

            $end     = clone $start;
            $hour    = (int)$module->getDuration()->format('G') + (int)$start->format('G');
            $minutes = (int)$module->getDuration()->format('i') + (int)$start->format('i');
            $end->setTime($hour, $minutes, 0);

            $dayOfWeek = $start->format('l');

            $availableStartTime = $start->format('H:i');
            $availableEndTime = $end->format('H:i');

            // don't allow book session if the learner's working hours doesn't available with the session date
            $availableCalendar = false;
            if ($profile->getWorkingHours()) {
                $currentAvailabilityLearner = null;
                foreach ($profile->getWorkingHours() as $key => $availabilityLearner) {
                    if ($dayOfWeek == $key){
                        // find the beginning and ending of day of the learner's working hours
                        if ((!$availabilityLearner['startTime'] || !$availabilityLearner['endTime']) && ($availabilityLearner['startTimeAfternoon'] && $availabilityLearner['endTimeAfternoon'])) {
                            $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTimeAfternoon'];
                            $currentAvailabilityLearner['ending'] = $availabilityLearner['endTimeAfternoon'];
                        } else if (($availabilityLearner['startTime'] && $availabilityLearner['endTime']) && (!$availabilityLearner['startTimeAfternoon'] || !$availabilityLearner['endTimeAfternoon'])) {
                            $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTime'];
                            $currentAvailabilityLearner['ending'] = $availabilityLearner['endTime'];
                        } else if ($availabilityLearner['startTime'] && $availabilityLearner['endTime'] && $availabilityLearner['startTimeAfternoon'] && $availabilityLearner['endTimeAfternoon']) {
                            $currentAvailabilityLearner['beginning'] = $availabilityLearner['startTime'];
                            $currentAvailabilityLearner['ending'] = $availabilityLearner['endTimeAfternoon'];
                        }

                        if (strtotime($availableStartTime) >= strtotime($currentAvailabilityLearner['beginning'])
                        && strtotime($availableEndTime) <= strtotime($currentAvailabilityLearner['ending'])) {
                            $availableCalendar = true;
                        }
                    }
                }
            }

            // don't allow book the session when the session is expired
            $availableSession = ($now <= $start) ? true : false;

            // how many booked learners
            $learnerBooked = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy(array(
                'module'        => $module,
                'moduleSession' => $session,
            ));

            $bookingModule = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner'       => $this->getCurrentUser(),
                    'module'        => $module,
                    'moduleSession' => $session,
                ));

            $assignments = (bool)$this->getDoctrine()->getRepository(AssigmentResourceSystem::class)
                ->findOneBy(array(
                    'module'        => $module,
                    'moduleSession' => $session,
                ));

            $availableModule = $this->isAvailableModule($module);

            // Get trainer from module assignment
            $trainer                = null;
            $availableBookClassroom = true;
            $availableBook = false;
            $moduleAssignment = $module->getAssigments();
            if (count($moduleAssignment)) {
                $moduleAssignment = $moduleAssignment[0];
                $trainer          = $moduleAssignment->getLiveResource();
                if ($trainer) {
                    // Checking this trainer has booked (and <> this module) by any learners? If No => Can book
                    $availableBook = !$this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                        ->isTrainerBooked($trainer, clone $start, clone $end, $module);
                    // not allow book when the trainer has more 2 sessions a day with different city and the same country
                    if ($classRoomTraining) {
                        $availableBookClassroom = $this->isAvailableBookingClassroomTraining($trainer, $module,
                            $session, clone $start);
                    }
                }
            }

            // Get street from module Session Place
            $address      = '';
            $sessionPlace = $session->getSessionPlace();
            if ($sessionPlace) {
                $address = $sessionPlace->getAddress() . ($sessionPlace->getPlace() ? ' - ' . $sessionPlace->getPlace() : '');
            }

            $canBooking = $assignments && $availableCalendar && $availableSession && $availableModule && $availableBook && $availableBookClassroom && $allowPersonalBook;

            $conditionsBook = [
                'assignments' => $assignments,
                'availableCalendar' => $availableCalendar,
                'availableSession' => $availableSession,
                'availableModule' => $availableModule,
                'availableBook' => $availableBook,
                'availableBookClassroom' => (bool)$availableBookClassroom,
                'allowPersonalBook' => (bool)$allowPersonalBook,
                ];

            array_push($sessionsDay, array(
                'start'                 => $start,
                'end'                   => $end,
                'startTime'                 => $start->format('H:i'),
                'endTime'                   => $end->format('H:i'),
                'title'                 => $start->format('H:i') . ' - ' . $end->format('H:i') . ' ',
                'duration'              => $module->getDuration()->format('G:i'),
                'session_id'            => $session->getId(),
                'session_date'          => $session->getSession()->getSessionDate(),
                'participants'          => $session->getSession()->getParticipantMax(),
                'participantsHasBooked' => count($learnerBooked),
                'hasBooked'             => null !== $bookingModule,
                'canBook'               => $canBooking,
                'displayName'           => $trainer ? $trainer->getDisplayName() : '',
                'mobile'                => $trainer ? $trainer->getMobilePhone() : '',
                'email'                 => $trainer ? $trainer->getPerson()->getEmail() : '',
                'address'               => $address,
                'conditionsBook'        => $conditionsBook,
            ));
        }

        $view = View::create();
        $view->setData($sessionsDay);
        return $this->handleView($view);
    }

    /**
     * POST postBookingAction.
     *
     * @Post("booking/online")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="booking_date", nullable=false, strict=true, description="Booking Date.")
     * @RequestParam(name="live_resource", nullable=false, strict=true, description="live Resource.")
     * @RequestParam(name="learner_id", nullable=false, strict=true, description="Learner ID.")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function postBookingOnlineAction(ParamFetcher $paramFetcher)
    {
        $moduleId     = $paramFetcher->get('module_id');
        $bookingDate  = $paramFetcher->get('booking_date');
        $liveResource = $paramFetcher->get('live_resource');
        $learnerId    = $paramFetcher->get('learner_id');

        if ($learnerId) {
            $learner = $this->getDoctrine()
                ->getRepository('ApiBundle:Profile')
                ->find($learnerId);
        } else {
            $learner = $this->getCurrentUser();
        }

        if (!$learner) {
            return $this->forbid();
        }

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $storedModule = $module->getStoredModule()->getAppId();

        if (StoredModule::ONLINE !== $storedModule) {
            return $this->errorHandler('Not an online module');
        }

        /** @var Profile $trainer */
        $trainer = $this->getProfileRepository()->find($liveResource);

        if (!$trainer) {
            return $this->errorHandler('No trainer found');
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);

        if (0 == count($errors)) {
            $em              = $this->getDoctrine()->getManager();
            $isBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                'learner' => $learner,
                'module'  => $module,
            ));

            if ($isBookingAgenda) {
                $em->remove($isBookingAgenda);
                $em->flush();
            }

            $bookingAgenda = new BookingAgenda();
            $bookingAgenda->setLearner($learner);
            $bookingAgenda->setTrainer($trainer);
            $bookingAgenda->setModule($module);
            $bookingAgenda->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::WITH_BOOKING_BOOKED,
            )));

            $this->setStatusModuleIteration($learner, $module, Status::WITH_BOOKING_BOOKED);

            $bookingDate = new DateTime($bookingDate);
            $bookingAgenda->setBookingDate($bookingDate);
            $em->persist($bookingAgenda);
            $em->flush();

            $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingAgenda);
            $this->get(Email::class)->sendMailBookingConfirm($learner, $module, $bookingAgenda);
            $view->setData(self::RESPONSE_OK);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postBookingAction.
     *
     * @Post("booking/session")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="session_id", nullable=false, strict=true, description="Session Id.")
     * @RequestParam(name="learner_id", nullable=false, strict=true, description="Learner Id.")
     *
     * @return Response
     */
    public function postBookingSessionAction(ParamFetcher $paramFetcher)
    {
        $moduleId  = $paramFetcher->get('module_id');
        $sessionId = $paramFetcher->get('session_id');
        $learnerId = $paramFetcher->get('learner_id');

        if ($learnerId) {
            $learner = $this->getDoctrine()
                ->getRepository('ApiBundle:Profile')
                ->find($learnerId);
        } else {
            $learner = $this->getCurrentUser();
        }

        if (!$learner) {
            return $this->forbid();
        }

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $storedModule = $module->getStoredModule()->getAppId();

        if ((StoredModule::VIRTUAL_CLASS !== $storedModule) &&
            (StoredModule::ONLINE_WORKSHOP !== $storedModule) &&
            (StoredModule::PRESENTATION_ANIMATION !== $storedModule)
        ) {
            return $this->errorHandler('Not a session module');
        }

        /** @var ModuleSession $moduleSession */
        $moduleSession = $this->getDoctrine()->getRepository('ApiBundle:ModuleSession')->findOneBy(array(
            'id' => $sessionId,
        ));

        if (!$moduleSession) {
            return $this->errorHandler('No session for this module');
        }

        $start   = $moduleSession->getSession()->getSessionDate();
        $end     = clone $start;
        $hour    = (int)$module->getDuration()->format('G') + (int)$moduleSession->getSession()->getSessionDate()->format('G');
        $minutes = (int)$module->getDuration()->format('i') + (int)$moduleSession->getSession()->getSessionDate()->format('i');
        $end->setTime($hour, $minutes, 0);

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $em              = $this->getDoctrine()->getManager();
            $isBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                'learner' => $learner,
                'module'  => $module,
            ));

            if ($isBookingAgenda) {
                $em->remove($isBookingAgenda);
                $em->flush();
            }

            //get trainer from module assignment
            $moduleAssignment = $module->getAssigments();
            if (!$moduleAssignment) {
                return $this->errorHandler('No assignment for this module');
            }
            $moduleAssignment = $moduleAssignment[0];
            $trainer          = $moduleAssignment->getLiveResource();
            //end get trainer from module assignment

            $bookingAgenda = new BookingAgenda();
            $bookingAgenda->setBookingDate($moduleSession->getSession()->getSessionDate());
            $bookingAgenda->setLearner($learner);
            $bookingAgenda->setTrainer($trainer);
            $bookingAgenda->setModule($module);

            $bookingAgenda->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::WITH_BOOKING_BOOKED,
            )));

            $this->setStatusModuleIteration($learner, $module, Status::WITH_BOOKING_BOOKED);

            $bookingAgenda->setModuleSession($moduleSession);
            $em->persist($bookingAgenda);
            $em->flush();

            // begin set Status::WITH_BOOKING_CONFIRMED status for all booked with Status::WITH_BOOKING_BOOKED status
            if (StoredModule::PRESENTATION_ANIMATION == $storedModule) {
                $participantMinimum = $moduleSession->getSession()->getParticipant();
                $bookingAgendas     = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                    'moduleSession' => $moduleSession,
                    'module'        => $module,
                    'status'        => $this->getDoctrine()->getRepository('ApiBundle:Status')->findBy(array(
                        'appId' => [Status::WITH_BOOKING_BOOKED, Status::WITH_BOOKING_CONFIRMED],
                    )),
                ]);

                if (count($bookingAgendas) >= $participantMinimum) {
                    foreach ($bookingAgendas as $booked) {
                        $this->setStatusModuleIteration($booked->getLearner(), $module, Status::WITH_BOOKING_CONFIRMED);
                        $booked->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                            'appId' => Status::WITH_BOOKING_CONFIRMED,
                        )));
                    }
                }
            }
            // end set Status::WITH_BOOKING_CONFIRMED status for all booked with Status::WITH_BOOKING_BOOKED status
            $em->flush();

            $this->get(Email::class)->sendMailBookingConfirm($trainer, $module, $bookingAgenda);
            $this->get(Email::class)->sendMailBookingConfirm($learner, $module, $bookingAgenda);

            $view->setData(self::RESPONSE_OK);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postUnBookingAction.
     *
     * @Post("unbooking")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="learner_id", nullable=false, strict=true, description="Learner Id.")
     *
     * @return Response
     */
    public function postUnBookingAction(ParamFetcher $paramFetcher)
    {
        $moduleId  = $paramFetcher->get('module_id');
        $learnerId = $paramFetcher->get('learner_id');

        if ($learnerId) {
            $profile = $this->getDoctrine()
                ->getRepository('ApiBundle:Profile')
                ->find($learnerId);
        } else {
            $profile = $this->getCurrentUser();
        }

        if (!$profile) {
            return $this->forbid();
        }

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $storedModuleAppId = $module->getStoredModule()->getAppId();

        if ((StoredModule::ONLINE !== $storedModuleAppId) &&
            (StoredModule::VIRTUAL_CLASS !== $storedModuleAppId) &&
            (StoredModule::ONLINE_WORKSHOP !== $storedModuleAppId) &&
            (StoredModule::PRESENTATION_ANIMATION !== $storedModuleAppId)
        ) {
            return $this->errorHandler('Not a booking module');
        }

        $isBookingAgenda = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'learner' => $profile,
            'module'  => $module,
        ));

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors) && $isBookingAgenda) {
            // set status cancel to send email to trainer
            $em = $this->getDoctrine()->getManager();
            $statusCanceled = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                'appId' => Status::WITH_BOOKING_CANCELED,
            ));
            $isBookingAgenda->setStatus($statusCanceled);
            $em->persist($isBookingAgenda);
            $em->flush();
            $this->get(Email::class)->sendMailBookingCancel($isBookingAgenda, false);
            // and delete it
            $em->remove($isBookingAgenda);
            $em->flush();

            $this->setStatusModuleIteration($profile, $module, Status::LEARNER_INTERVENTION_CREATED);

            // begin set Status::WITH_BOOKING_BOOKED status for all booked with Status::WITH_BOOKING_CONFIRMED status
            if (StoredModule::PRESENTATION_ANIMATION == $storedModuleAppId) {
                $participantMinimum = $isBookingAgenda->getModuleSession()->getSession()->getParticipant();
                $confirmedBooking   = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findBy([
                    'moduleSession' => $isBookingAgenda->getModuleSession(),
                    'module'        => $module,
                    'status'        => $this->getDoctrine()->getRepository('ApiBundle:Status')->findBy(array(
                        'appId' => [Status::WITH_BOOKING_CONFIRMED, Status::WITH_BOOKING_BOOKED],
                    )),
                ]);

                if (count($confirmedBooking) < $participantMinimum) {
                    foreach ($confirmedBooking as $confirmed) {
                        $this->setStatusModuleIteration($confirmed->getLearner(), $module, Status::WITH_BOOKING_BOOKED);
                        $confirmed->setStatus($this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                            'appId' => Status::WITH_BOOKING_BOOKED,
                        )));
                    }
                }
            }
            // end set Status::WITH_BOOKING_BOOKED status for all booked with Status::WITH_BOOKING_CONFIRMED status
            $em->flush();
            $view->setData('OK');
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * Put StartAction.
     *
     * @Put("start/{id}")
     *
     * @param $id
     *
     * @return Response
     */
    public function putStartAction($id)
    {
        $learnerIntervention = $this->getLearnerInterventionRepository()->findOneBy(array(
            'id'      => $id,
            'learner' => $this->getCurrentUser(),
        ));

        if (!$learnerIntervention) {
            $this->notFound();
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($learnerIntervention);
        if (0 == count($errors)) {
            $learnerIntervention->setStatus(
                $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS,
                ))
            );
            $em = $this->getDoctrine()->getManager();
            $em->persist($learnerIntervention);
            $em->flush();
            $view->setData($learnerIntervention->getIntervention()->getId());
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * @Post("progress")
     * POST postProgressAction
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="intervention_id", nullable=false, strict=true, description="Intervention Id.")
     *
     * @return Response
     */
    public function postProgressAction(ParamFetcher $paramFetcher)
    {
        $moduleId        = $paramFetcher->get('module_id');
        $intervention_id = $paramFetcher->get('intervention_id');

        $em     = $this->getDoctrine()->getManager();
        $module = $em->getRepository('ApiBundle:Module')
            ->find($moduleId);

        if (!$module) {
            return $this->notFound();
        }

        $profile = $this->getCurrentUser();
        if (!$profile) {
            return $this->forbid();
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $intervention        = $em->getRepository('ApiBundle:Intervention')
                ->find($intervention_id);
            $interventionLearner = $em->getRepository('ApiBundle:LearnerIntervention')
                ->findOneBy(['learner' => $profile, 'intervention' => $intervention]);

            //Up the progression
            $progress = $interventionLearner->getProgression();
            $nbModule = count($em->getRepository('ApiBundle:Module')
                    ->findBy(['intervention' => $intervention])) - 2;

            $nbModuleDone = round(($progress * $nbModule) / 100, 0, PHP_ROUND_HALF_UP);

            ++$nbModuleDone;

            $progress = ($nbModuleDone / $nbModule) * 100;

            $interventionLearner->setProgression($progress);
            if (100 == $progress) {
                $interventionLearner->setStatus($em->getRepository('ApiBundle:Status')
                    ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_FINISHED]));
            }

            //Set the iteration
            $status = $em->getRepository('ApiBundle:Status')
                ->findOneBy(['appId' => Status::WITHOUT_BOOKING_DONE]);
            $module->setStatus($status);
            $moduleIteration = new ModuleIteration();
            $moduleIteration->setLearner($profile);
            $moduleIteration->setModule($module);
            $moduleIteration->setExecutionDate(new DateTime());
            $em->persist($interventionLearner);
            $em->persist($moduleIteration);
            $em->flush();
            $view->setData($moduleIteration);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postNotationAction.
     *
     * @Post("notation")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="designation", nullable=false, strict=true, description="Message.")
     * @RequestParam(name="notation", nullable=false, strict=true, description="Notation.")
     * @RequestParam(name="notationTrainer", nullable=true, strict=true, description="notationTrainer.")
     * @RequestParam(name="designationTrainer", nullable=true, strict=true, description="designationTrainer.")
     * @RequestParam(name="startTime", nullable=true, strict=true, description="startTime.")
     * @RequestParam(name="learnedTime", nullable=true, strict=true, description="learnedTime.")
     *
     * @return Response
     */
    public function postNotationAction(ParamFetcher $paramFetcher)
    {
        $moduleId           = $paramFetcher->get('module_id');
        $designation        = $paramFetcher->get('designation');
        $notation           = $paramFetcher->get('notation');
        $notationTrainer    = $paramFetcher->get('notationTrainer');
        $designationTrainer = $paramFetcher->get('designationTrainer');

        $startTime   = $paramFetcher->get('startTime');
        $learnedTime = $paramFetcher->get('learnedTime');
        $learnedTime = (int)$learnedTime;

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $profile = $this->getCurrentUser();
        if (!$profile) {
            return $this->forbid();
        }

        if (!$this->canModuleBeNotated($module, $profile)) {
            return $this->forbid('You have to finish previous modules');
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $em          = $this->getDoctrine()->getManager();
            $moduleAppId = $module->getStoredModule()->getAppId();

            $moduleResponse = new ModuleResponse();
            $moduleResponse->setLearner($profile);
            $moduleResponse->setModule($module);
            $moduleResponse->setDateResponse(new DateTime());
            $moduleResponse->setDesignation($designation);
            $moduleResponse->setNotation((float)$notation);
            //trainer rating
            if ($moduleAppId == StoredModule::PRESENTATION_ANIMATION || $moduleAppId == StoredModule::ONLINE || $moduleAppId == StoredModule::VIRTUAL_CLASS) {
                $bookingModule = $this->getDoctrine()
                    ->getRepository('ApiBundle:BookingAgenda')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));
                if ($moduleAppId == StoredModule::ONLINE || $moduleAppId == StoredModule::VIRTUAL_CLASS) {
                    $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
                        ->findOneBy(array(
                            'learner' => $profile,
                            'module'  => $module,
                        ));
                    $status          = $em->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);
                    $moduleIteration->setStatus($status);
                    $em->persist($moduleIteration);
                    $bookingStatus = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                        'appId' => Status::WITH_BOOKING_IN_PROGRESS,
                    ));
                    $bookingModule->setStatus($bookingStatus);
                    $em->persist($bookingModule);
                }

                $moduleTrainerResponse = new ModuleTrainerResponse();
                $moduleTrainerResponse->setTrainer($bookingModule->getTrainer());
                $moduleTrainerResponse->setModule($module);
                $moduleTrainerResponse->setDateResponse(new DateTime());
                $moduleTrainerResponse->setDesignation($designationTrainer);
                $moduleTrainerResponse->setNotation((float)$notationTrainer);
                $moduleTrainerResponse->setLearner($profile);
                $em->persist($moduleTrainerResponse);
            }
            //trainer rating

            if ($moduleAppId == StoredModule::ELEARNING || $moduleAppId == StoredModule::QUIZ || $moduleAppId == StoredModule::EVALUATION) {
                $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));

                if (!isset($moduleIteration)) {
                    $moduleIteration = new ModuleIteration();
                    $moduleIteration->setLearner($profile);
                    $moduleIteration->setModule($module);
                    //$moduleIteration->setStartTime(new DateTime($startTime));
                }
                $moduleIteration->setExecutionDate(new DateTime());
                //$moduleIteration->setLearnedTime($learnedTime);
                $status = null;
                /*//set done status for module
                if ($moduleAppId == StoredModule::EVALUATION && $quiz = $module->getEchoModule()->getEchoId()) {
                    $organizationId = $this->getParameter('echo_organization_id');
                    try {
                        // Get Program by email and program id
                        // https://integration.surge9.com/api/integration/C6DB6CE0-4609-4BC5-9BF9-AC40C1D78963/learnerprogress?programId=70587823&email=learner@deepmark.fr
                        $client = new \GuzzleHttp\Client(['headers' => ['x-api-key' => $this->getParameter('echo_api_key'),
                            'Content-Type' => 'application/json']]);
                        $urlProgram = $this->getParameter('echo_url') . '/api/integration/'.$organizationId.'/learnerprogress?programId=' . $module->getEchoModule()->getEchoId() . '&email=' . $profile->getPerson()->getEmail();
                        $responseProgram = $client->request('GET', $urlProgram);
                        $program = json_decode($responseProgram->getBody(), true);
                        if (isset($program['learners'][0]['programs'][0]['learningJourney']['completionStatus']) && $program['learners'][0]['programs'][0]['learningJourney']['completionStatus'] = 'In Progress') {
                            $status = $em->getRepository('ApiBundle:Status')
                                ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);
                        } else if (isset($program['learners'][0]['programs'][0]['learningJourney']['completionStatus']) && $program['learners'][0]['programs'][0]['learningJourney']['completionStatus'] = 'In Progress') {
                            $status = $em->getRepository('ApiBundle:Status')
                                ->findOneBy(['appId' => Status::WITH_BOOKING_DONE]);
                        }
                    } catch (Exception $e) {
                        // echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }
                } else {
                    $status = $em->getRepository('ApiBundle:Status')
                        ->findOneBy(['appId' => Status::WITH_BOOKING_DONE]);
                }*/
                $status = $em->getRepository('ApiBundle:Status')
                    ->findOneBy(['appId' => Status::WITH_BOOKING_DONE]);
                $moduleIteration->setStatus($status);
                $em->persist($moduleIteration);
                //end set done status for module

                // if it is SCORM
                if (!empty($module->getMediaDocument()) && !empty($module->getMediaDocument()->getFileDescriptor())
                    && $module->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
                    $scormData = $this->getResultScorm($module->getId());
                    if ($scormData) {
                        $scormSynthesis = new ScormSynthesis();
                        $scormSynthesis->setProfile($profile);
                        $scormSynthesis->setSCOInstanceID($module->getId());
                        if ($scormData['lessonStatus']) {
                            $scormSynthesis->setLessonStatus($scormData['lessonStatus']);
                        }
                        if ($scormData['rawsSore']) {
                            $scormSynthesis->setScore($scormData['rawsSore']);
                        }
                        if ($scormData['totalTime']) {
                            $scormSynthesis->setTimeSpent($scormData['totalTime']);
                        }
                        $em->persist($scormSynthesis);
                    }
                }
            }

            $em->persist($moduleResponse);
            $em->flush();
            $view->setData(self::RESPONSE_OK);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postDoneAction.
     *
     * @Post("done")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     *
     * @return Response
     */
    public function postDoneAction(ParamFetcher $paramFetcher)
    {
        $moduleId           = $paramFetcher->get('module_id');

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $profile = $this->getCurrentUser();
        if (!$profile) {
            return $this->forbid();
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            // set status as Done when user click next module
            $moduleIterationMustDone = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $module,
                ));

            if (isset($moduleIterationMustDone)) {
                $statusDone = $this->getDoctrine()->getRepository('ApiBundle:Status')->findOneBy(array(
                    'appId' => Status::WITH_BOOKING_DONE,
                ));
                $moduleIterationMustDone->setStatus($statusDone);
                $moduleIterationMustDone->setExecutionDate(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($moduleIterationMustDone);
                $em->flush();
            }

            $view->setData(self::RESPONSE_OK);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postTimingAction.
     *
     * @Post("timing")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="startTime", nullable=true, strict=true, description="startTime.")
     * @RequestParam(name="learnedTime", nullable=true, strict=true, description="learnedTime.")
     *
     * @return Response
     */
    public function postTimingAction(ParamFetcher $paramFetcher)
    {
        $moduleId    = $paramFetcher->get('module_id');
        $startTime   = $paramFetcher->get('startTime');
        $learnedTime = $paramFetcher->get('learnedTime');
        $learnedTime = (int)$learnedTime;

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $profile = $this->getCurrentUser();
        if (!$profile) {
            return $this->forbid();
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $em          = $this->getDoctrine()->getManager();
            $moduleAppId = $module->getStoredModule()->getAppId();

            // save the learned time of profile
            $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $module,
                ));
            if (!isset($moduleIteration)) {
                $moduleIteration = new ModuleIteration();
                $moduleIteration->setLearner($profile);
                $moduleIteration->setModule($module);
                $moduleIteration->setStartTime(new DateTime($startTime));
                $moduleIteration->setLearnedTime($learnedTime);
            } else {
                $total = $moduleIteration->getLearnedTime() + $learnedTime;
                $moduleIteration->setLearnedTime($total);
            }
            $moduleIteration->setExecutionDate(new DateTime());
            $em->persist($moduleIteration);

            if ($moduleAppId == StoredModule::ELEARNING) {
                // if it is SCORM
                if (!empty($module->getMediaDocument()) && !empty($module->getMediaDocument()->getFileDescriptor())
                    && $module->getMediaDocument()->getFileDescriptor()->getMimeType() == 'zip') {
                    $scormData = $this->getResultScorm($module->getId());
                    if ($scormData) {
                        $scormSynthesis = new ScormSynthesis();
                        $scormSynthesis->setProfile($profile);
                        $scormSynthesis->setSCOInstanceID($module->getId());
                        if ($scormData['lessonStatus']) {
                            $scormSynthesis->setLessonStatus($scormData['lessonStatus']);
                        }
                        if ($scormData['rawsSore']) {
                            $scormSynthesis->setScore($scormData['rawsSore']);
                        }
                        if ($scormData['totalTime']) {
                            $scormSynthesis->setTimeSpent($scormData['totalTime']);
                        }
                        $em->persist($scormSynthesis);
                    }
                }
            }
            $em->flush();
            $view->setData(self::RESPONSE_OK);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postStatusAction.
     *
     * @Post("status")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="bookingId", nullable=false, strict=true, description="Booking Id.")
     * @RequestParam(name="status", nullable=false, strict=true, description="status.")
     *
     * @return Response
     */
    public function postStatusAction(ParamFetcher $paramFetcher)
    {
        $bookingId    = $paramFetcher->get('bookingId');
        $status   = $paramFetcher->get('status');

        /** @var Module $module */
        $booking = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
            'id' => $bookingId,
        ));

        if (!$booking) {
            return $this->notFound();
        }

        $profile = $this->getCurrentUser();
        if (!$profile) {
            return $this->forbid();
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($booking);
        if (0 == count($errors)) {
            $em          = $this->getDoctrine()->getManager();

            $booking->setJoinStatus($status);
            $em->persist($booking);
            $em->flush();

            $view->setData(self::RESPONSE_OK);
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postEchoProgressAction.
     *
     * @Post("echo-progress")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="module_id", nullable=false, strict=true, description="Module Id.")
     * @RequestParam(name="startTime", nullable=true, strict=true, description="startTime.")
     * @RequestParam(name="learnedTime", nullable=true, strict=true, description="learnedTime.")
     *
     * @return Response
     */
    public function postEchoProgressAction(ParamFetcher $paramFetcher)
    {
        $moduleId    = $paramFetcher->get('module_id');
        $startTime   = $paramFetcher->get('startTime');
        $learnedTime = $paramFetcher->get('learnedTime');

        /** @var Module $module */
        $module = $this->getDoctrine()->getRepository('ApiBundle:Module')->findOneBy(array(
            'id' => $moduleId,
        ));

        if (!$module) {
            return $this->notFound();
        }

        $profile = $this->getCurrentUser();
        if (!$profile) {
            return $this->forbid();
        }

        $view   = View::create();
        $errors = $this->get('validator')->validate($module);
        if (0 == count($errors)) {
            $em          = $this->getDoctrine()->getManager();
            $moduleAppId = $module->getStoredModule()->getAppId();

            if ($moduleAppId == StoredModule::EVALUATION && $quiz = $module->getEchoModule()->getEchoId()) {
                $moduleIteration = $em->getRepository('ApiBundle:ModuleIteration')
                    ->findOneBy(array(
                        'learner' => $profile,
                        'module'  => $module,
                    ));

                if (!isset($moduleIteration)) {
                    $moduleIteration = new ModuleIteration();
                    $moduleIteration->setLearner($profile);
                    $moduleIteration->setModule($module);
                    $moduleIteration->setExecutionDate(new DateTime());
                    $moduleIteration->setStartTime(new DateTime($startTime));
                    $moduleIteration->setLearnedTime($learnedTime);
                } else {
                    $moduleIteration->setLearnedTime($moduleIteration->getLearnedTime() + $learnedTime);
                }

                $status         = null;
                $organizationId = $this->getParameter('echo_organization_id');
                try {
                    // Get Program by email and program id
                    // https://integration.surge9.com/api/integration/C6DB6CE0-4609-4BC5-9BF9-AC40C1D78963/learnerprogress?programId=70587823&email=learner@deepmark.fr
                    $client          = new \GuzzleHttp\Client([
                        'headers' => [
                            'x-api-key'    => $this->getParameter('echo_api_key'),
                            'Content-Type' => 'application/json',
                        ],
                    ]);
                    $urlProgram      = $this->getParameter('echo_url') . '/api/integration/' . $organizationId . '/learnerprogress?programId=' . $module->getEchoModule()->getEchoId() . '&email=' . $profile->getPerson()->getEmail();
                    $responseProgram = $client->request('GET', $urlProgram);
                    $program         = json_decode($responseProgram->getBody(), true);
                    if (isset($program['learners'][0]['programs'][0]['learningJourney']['completionStatus']) && $program['learners'][0]['programs'][0]['learningJourney']['completionStatus'] = 'In Progress') {
                        $status = $em->getRepository('ApiBundle:Status')
                            ->findOneBy(['appId' => Status::LEARNER_INTERVENTION_IN_PROGRESS]);
                    } else {
                        if (isset($program['learners'][0]['programs'][0]['learningJourney']['completionStatus']) && $program['learners'][0]['programs'][0]['learningJourney']['completionStatus'] = 'In Progress') {
                            $status = $em->getRepository('ApiBundle:Status')
                                ->findOneBy(['appId' => Status::WITH_BOOKING_DONE]);
                        }
                    }
                } catch (Exception $e) {
                    // echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                $moduleIteration->setStatus($status);
                $em->persist($moduleIteration);
            }
            $em->flush();
            $view->setData('Done');
            $view->setStatusCode(200);
        } else {
            return $this->missing('Error :', $errors);
        }

        return $this->handleView($view);
    }

    /**
     * POST postScormCommit.
     *
     * @Post("scorm/commit")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="SCOInstanceID", nullable=false, strict=true, description="SCOInstanceID.")
     * @RequestParam(name="data", nullable=false, strict=true, description="Data.")
     *
     * @return Response
     */
    public function postScormCommitAction(ParamFetcher $paramFetcher)
    {
        $SCOInstanceID = $paramFetcher->get('SCOInstanceID');
        $data          = $paramFetcher->get('data');

        if (!is_array($data)) {
            $data = array($data);
        }

        // iterate through the data elements
        foreach ($data as $varName => $varValue) {

            // save data to the 'scormvars' table
            $this->writeElement($SCOInstanceID, $varName, $varValue);

            // special cases - set appropriate values in the LMS tables when they are set by the course
            if ($varName == "cmi.core.score.raw") {
                $this->setInLMS('TestScore', $varValue);
            }
            if ($varName == "cmi.core.lesson_status") {
                $this->setInLMS('Finished', $varValue);
            }

        }

        $dataScrom = $this->getResultScorm($SCOInstanceID);
        $view      = View::create();
        $view->setData($dataScrom);
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    /**
     * POST postScorm2004Commit.
     *
     * @Post("scorm2004/commit")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @RequestParam(name="SCOInstanceID", nullable=false, strict=true, description="SCOInstanceID.")
     * @RequestParam(name="data", nullable=false, strict=true, description="Data.")
     *
     * @return Response
     */
    public function postScorm2004CommitAction(ParamFetcher $paramFetcher)
    {
        $SCOInstanceID = $paramFetcher->get('SCOInstanceID');
        $data          = $paramFetcher->get('data');

        if (!is_array($data)) {
            $data = array($data);
        }

        // iterate through the data elements
        foreach ($data as $varName => $varValue) {
            // save data to the 'scormvars' table
            $this->writeElement($SCOInstanceID, $varName, $varValue);
        }

        $dataScrom = $this->getResultScorm($SCOInstanceID);
        $view      = View::create();
        $view->setData($dataScrom);
        $view->setStatusCode(200);

        return $this->handleView($view);
    }

    /**
     * GET postScormFinish.
     *
     * @Get("scorm/finish")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @QueryParam(name="SCOInstanceID", nullable=true, description="SCOInstanceID")
     * @QueryParam(name="code", nullable=true, description="Code")
     *
     * @return Response
     */
    public function getScormFinishAction(ParamFetcher $paramFetcher)
    {
        $SCOInstanceID = $paramFetcher->get('SCOInstanceID');
        $lessonStatus  = '';
        $rawsSore      = 0;
        // ------------------------------------------------------------------------------------
        // set cmi.core.lesson_status

        // find existing value of cmi.core.lesson_status
        $lessonStatus = $this->readElement($SCOInstanceID, 'cmi.core.lesson_status');
        // if it's 'not attempted', change it to 'completed'
        if ($lessonStatus == 'not attempted') {
            $this->writeElement($SCOInstanceID, 'cmi.core.lesson_status', 'completed');
        }

        // has a mastery score been specified in the IMS manifest file?
        $masteryScore = $this->readElement($SCOInstanceID, 'adlcp:masteryscore');
        $masteryScore *= 1;

        if ($masteryScore) {

            // yes - so read the score
            $rawsSore = $this->readElement($SCOInstanceID, 'cmi.core.score.raw');
            $rawsSore *= 1;

            // set cmi.core.lesson_status to passed/failed
            if ($rawsSore >= $masteryScore) {
                $lessonStatus = 'passed';
                $this->writeElement($SCOInstanceID, 'cmi.core.lesson_status', 'passed');
            } else {
                $lessonStatus = 'failed';
                $this->writeElement($SCOInstanceID, 'cmi.core.lesson_status', 'failed');
            }

        }

        // ------------------------------------------------------------------------------------
        // set cmi.core.entry based on the value of cmi.core.exit

        // clear existing value
        $this->writeElement($SCOInstanceID, 'cmi.core.entry', '');

        // new entry value depends on exit value
        $exit = $this->readElement($SCOInstanceID, 'cmi.core.exit');
        if ($exit == 'suspend') {
            $this->writeElement($SCOInstanceID, 'cmi.core.entry', 'resume');
        } else {
            $this->writeElement($SCOInstanceID, 'cmi.core.entry', '');
        }

        // ------------------------------------------------------------------------------------
        // process changes to cmi.core.total_time

        // read cmi.core.total_time from the 'scormvars' table
        $totalTime = $this->readElement($SCOInstanceID, 'cmi.core.total_time');
        // convert total time to seconds
        $time         = explode(':', $totalTime);
        $totalSeconds = $time[0] * 60 * 60 + $time[1] * 60 + $time[2];

        // read the last-set cmi.core.session_time from the 'scormvars' table
        $sessionTime = $this->readElement($SCOInstanceID, 'cmi.core.session_time');

        // no session time set by SCO - set to zero
        if (!$sessionTime) {
            $sessionTime = "00:00:00";
        }

        // convert session time to seconds
        $time           = explode(':', $sessionTime);
        $sessionSeconds = $time[0] * 60 * 60 + $time[1] * 60 + $time[2];

        // new total time is ...
        $totalSeconds += $sessionSeconds;

        // break total time into hours, minutes and seconds
        $totalHours   = intval($totalSeconds / 3600);
        $totalSeconds -= $totalHours * 3600;
        $totalMinutes = intval($totalSeconds / 60);
        $totalSeconds -= $totalMinutes * 60;

        // reformat to comply with the SCORM data model
        $totalTime = sprintf("%04d:%02d:%02d", $totalHours, $totalMinutes, $totalSeconds);

        // save new total time to the 'scormvars' table
        $this->writeElement($SCOInstanceID, 'cmi.core.total_time', $totalTime);

        // delete the last session time
        $this->writeElement($SCOInstanceID, 'cmi.core.session_time', '');

        $view = View::create();
        $view->setData([
            'rawsSore'     => $rawsSore,
            'lessonStatus' => $lessonStatus,
            'totalTime'    => $totalTime,
        ]);
        $view->setStatusCode(200);

        return $this->handleView($view);
    }
}
