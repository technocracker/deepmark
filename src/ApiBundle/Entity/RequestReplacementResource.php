<?php

namespace ApiBundle\Entity;

/**
 * RequestReplacementResource.
 */
class RequestReplacementResource
{
    const STATUS_UNKNOWN = 'UNKNOWN';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_DENY = 'DENY';
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var \DateTime|null
     */
    private $validationDate;

    /**
     * @var string
     */
    private $status = 'UNKNOWN';

    /**
     * @var \ApiBundle\Entity\BookingAgenda
     */
    private $bookingAgenda;

    /**
     * @var Session
     */
    private $session;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAdd.
     *
     * @param \DateTime $dateAdd
     *
     * @return RequestReplacementResource
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd.
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set validationDate.
     *
     * @param \DateTime|null $validationDate
     *
     * @return RequestReplacementResource
     */
    public function setValidationDate($validationDate = null)
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    /**
     * Get validationDate.
     *
     * @return \DateTime|null
     */
    public function getValidationDate()
    {
        return $this->validationDate;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return RequestReplacementResource
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\BookingAgenda|null $bookingAgenda
     *
     * @return RequestReplacementResource
     */
    public function setBookingAgenda(\ApiBundle\Entity\BookingAgenda $bookingAgenda = null)
    {
        $this->bookingAgenda = $bookingAgenda;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\BookingAgenda|null
     */
    public function getBookingAgenda()
    {
        return $this->bookingAgenda;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param Session|null $session
     * @return $this
     */
    public function setSession(Session $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return RequestReplacementResource
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return RequestReplacementResource
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
