<?php

namespace ApiBundle\Entity;

/**
 * Regional.
 */
class Regional
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \ApiBundle\Entity\Entity
     */
    private $entity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrganizations;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add groupOrganization.
     *
     * @param \ApiBundle\Entity\GroupOrganization $groupOrganization
     *
     * @return Regional
     */
    public function addGroupOrganization(\ApiBundle\Entity\GroupOrganization $groupOrganization)
    {
        $this->groupOrganizations[] = $groupOrganization;

        return $this;
    }

    /**
     * Remove groupOrganization.
     *
     * @param \ApiBundle\Entity\GroupOrganization $groupOrganization
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeGroupOrganization(\ApiBundle\Entity\GroupOrganization $groupOrganization)
    {
        return $this->groupOrganizations->removeElement($groupOrganization);
    }

    /**
     * Get groupOrganizations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupOrganizations()
    {
        return $this->groupOrganizations;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return Entity
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set entity.
     *
     * @param \ApiBundle\Entity\Entity|null $entity
     *
     * @return Entity
     */
    public function setEntity(\ApiBundle\Entity\Entity $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return \ApiBundle\Entity\Entity|null
     */
    public function getEntity()
    {
        return $this->entity;
    }

    public function __toString()
    {
        $name = '';
        if ($this->getEntity()) {
            $name .= $this->getEntity()->getDesignation().' ';
        }
        if ($this->getDesignation()) {
            $name .= $this->getDesignation();
        }

        return $name;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Entity
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Entity
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
