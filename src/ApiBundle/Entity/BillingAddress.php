<?php

namespace ApiBundle\Entity;

/**
 * BillingAddress.
 */
class BillingAddress
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string|null
     */
    private $expansionStreet;

    /**
     * @var string
     */
    private $siret;

    /**
     * @var string
     */
    private $tvaNumber;

    /**
     * @var float
     */
    private $billingCode;

    /**
     * @var \ApiBundle\Entity\Profile
     */
    private $profile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return BillingAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set expansionStreet.
     *
     * @param string|null $expansionStreet
     *
     * @return BillingAddress
     */
    public function setExpansionStreet($expansionStreet = null)
    {
        $this->expansionStreet = $expansionStreet;

        return $this;
    }

    /**
     * Get expansionStreet.
     *
     * @return string|null
     */
    public function getExpansionStreet()
    {
        return $this->expansionStreet;
    }

    /**
     * Set siret.
     *
     * @param string $siret
     *
     * @return BillingAddress
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret.
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set tvaNumber.
     *
     * @param string $tvaNumber
     *
     * @return BillingAddress
     */
    public function setTvaNumber($tvaNumber)
    {
        $this->tvaNumber = $tvaNumber;

        return $this;
    }

    /**
     * Get tvaNumber.
     *
     * @return string
     */
    public function getTvaNumber()
    {
        return $this->tvaNumber;
    }

    /**
     * Set billingCode.
     *
     * @param float $billingCode
     *
     * @return BillingAddress
     */
    public function setBillingCode($billingCode)
    {
        $this->billingCode = $billingCode;

        return $this;
    }

    /**
     * Get billingCode.
     *
     * @return float
     */
    public function getBillingCode()
    {
        return $this->billingCode;
    }

    /**
     * Set profile.
     *
     * @param \ApiBundle\Entity\Profile|null $profile
     *
     * @return BillingAddress
     */
    public function setProfile(\ApiBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \ApiBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return BillingAddress
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return BillingAddress
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @var string
     */
    private $country;

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return BillingAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipCode.
     *
     * @param string $zipCode
     *
     * @return BillingAddress
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return BillingAddress
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
}
