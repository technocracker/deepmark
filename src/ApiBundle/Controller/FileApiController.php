<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\ProfileDocument;
use AppBundle\Service\FileApi;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use DateTime;

class FileApiController extends RestController
{
    public const FILE_EDITOR_DIR = __DIR__.'/../../../web/files/editor';

    /**
     * GET list files by type.
     *
     * @param $appId
     * @param ParamFetcher $paramFetcher
     * @Rest\QueryParam(name="type", nullable=false, description="file type")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getFileTypeAction($appId)
    {
        $type = $this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
            'appId' => $appId,
        ));

        $files = $this->getDoctrine()->getRepository('ApiBundle:EducationalDocument')->findBy(array(
            'educationalDocVariety' => $type,
        ));

        $view = View::create();

        $view->setData($files);

        return $this->handleView($view);
    }

    /**
     * Upload File.
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @param Request      $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @FileParam(name="file", default="null")
     */
    public function postFileAction(ParamFetcher $paramFetcher)
    {
        $file = $paramFetcher->get('file');

        $fileApi = $this->get(FileApi::class);

        $view = View::create();
        if ($file) {
            $mimeType = explode('/', $file->getClientMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    $fileDescriptor = $fileApi->uploadPublicFile($file);
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    $fileDescriptor = $fileApi->uploadPublicFile($file);
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    $fileDescriptor = $fileApi->uploadPrivateFile($file);
                    break;
            }

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $em->flush();

            $document = new EducationalDocument();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);

            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();
            $view->setData($document);
            $view->setStatusCode(200);
        } else {
            return $this->errorHandler('Aucun fichier soumis');
        }

        return $this->handleView($view);
    }

    /**
     * Upload File.
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     *
     * @FileParam(name="file", default="null")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postFilePublicAction(ParamFetcher $paramFetcher)
    {
        $file = $paramFetcher->get('file');

        $fileApi = $this->get(FileApi::class);

        $view = View::create();
        if ($file) {
            $fileDescriptor = $fileApi->uploadPublicFile($file);
            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $em->flush();

            $document = new EducationalDocument();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();
            $view->setData($document);
            $view->setStatusCode(200);
        } else {
            return $this->errorHandler('Aucun fichier soumis');
        }

        return $this->handleView($view);
    }

    /**
     * Upload File.
     *
     * @Rest\Post("upload-editor-file")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     * @FileParam(name="file", default="null")
     */
    public function postEditorFileAction(ParamFetcher $paramFetcher)
    {
        $file = $paramFetcher->get('file');
        $fileApi = $this->get(FileApi::class);

        $view = View::create();
        if ($file) {
            $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
                'person' => $this->getUser(),
            ));
            $fileDescriptor = $fileApi->uploadEditorFile($file, $profile);
            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $em->flush();

            $profileDocument = new ProfileDocument();
            $profileDocument->setProfile($profile);
            $profileDocument->setFileDescriptor($fileDescriptor);
            $profileDocument->setDocumentType(ProfileDocument::EDITOR_FILE);
            $profileDocument->setCreated(new DateTime());
            $profileDocument->setUpdated(new DateTime());
            $em->persist($profileDocument);
            $em->flush();

            $view->setData($fileDescriptor);
            $view->setStatusCode(200);
        } else {
            return $this->errorHandler('Aucun fichier soumis');
        }

        return $this->handleView($view);
    }

    /**
     * Get Editor File.
     *
     * @Rest\Post("editor-files")
     *
     * @param ParamFetcher $paramFetcher ParamFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     * @FileParam(name="file", default="null")
     */
    public function getEditorFilesAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $this->getUser(),
        ));

        $profileDocuments = $this->getDoctrine()->getRepository('ApiBundle:ProfileDocument')->findBy(array(
            'profile' => $profile,
            'documentType' => ProfileDocument::EDITOR_FILE,
        ));

        $baseUrl = $this->container->get('router')->getContext()->getScheme() .'://'. $this->container->get('router')->getContext()->getHost().'/files/editor/'.$profile->getId().'/';

        $files = [];
        foreach ($profileDocuments as $file) {
            $thumb = null;
            if ($file->getFileDescriptor()->getThumbnail()) {
                $pieces = explode("../web/files/editor/".$profile->getId()."/", $file->getFileDescriptor()->getThumbnail());
                $thumb = $pieces[1];
            }
            $files[] = [
                'file' => $file->getFileDescriptor()->getPath(),
                'thumb' => $thumb ? $thumb : $file->getFileDescriptor()->getPath(),
                'changed' => $file->getFileDescriptor()->getUpdated(),
                'size' => $file->getFileDescriptor()->getSize(),
            ];
        }

        $data = [
            'success' => true,
            'time' => new DateTime(),
            'data' => ['sources' =>
                            ['files' =>
                                [
                                    'baseurl' => $baseUrl,
                                    'path' => '',
                                    'files' => $files
                                ]
                            ]
                      ],
            ];

        $view->setData($data);
        $view->setStatusCode(200);

        return $this->handleView($view);
    }
}
