<?php

namespace ApiBundle\Service\Alerts;

use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\ModuleReports;
use ApiBundle\Entity\ModuleSend;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\RequestReplacementResource;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\Status;
use ApiBundle\Entity\StoredModule;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Utils\Email;

/**
 * Class AlertService
 * @package ApiBundle\Service\Alerts
 */
class AlertService extends ContainerAwareCommand
{
    private $translator;
    private $entityManager;
    private $bookingAgendaRepository;
    private $availableCalendarRepository;
    private $statusRepository;
    private $moduleReportRepository;
    private $moduleSendRepository;

    /**
     * AlertService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, $translator = null)
    {
        parent::__construct();
        $this->setName('alertservice');
        $this->entityManager               = $entityManager;
        $this->bookingAgendaRepository     = $entityManager->getRepository(BookingAgenda::class);
        $this->learnerInterventionRepository = $entityManager->getRepository(LearnerIntervention::class);
        $this->availableCalendarRepository = $entityManager->getRepository(AvailableCalendar::class);
        $this->statusRepository            = $entityManager->getRepository(Status::class);
        $this->moduleReportRepository      = $entityManager->getRepository(ModuleReports::class);
        $this->moduleSendRepository        = $entityManager->getRepository(ModuleSend::class);
        $this->translator                  = $translator;
    }

    /**
     * @param $container
     * @param $hours
     * @param $domain
     * @throws Exception
     */
    public function alertNextSessions($container, $hours, $minutes, $type, $domain)
    {
        $logger = $container->get('logger');
        $bookings = $this->bookingAgendaRepository->findBookingForAlert($hours, $minutes);
        $logger->info('Alert next session: total bookings ' . count($bookings));
        $sent = [];
        foreach ($bookings as $booking) {
            if ($type == 1) { // only sent to learner
                $container->get(Email::class)->sendEmailNextSession($booking, $hours, $minutes, 1, $domain);
                $logger->info('Alert next session: send email to booking id '.$booking->getId() . ' with type ' . $type);

            } else if ($type == 2) { // only sent to trainer
                $string = $booking->getTrainer()->getId() . "-" . $booking->getModule()->getId() . "" . $booking->getBookingDate()->format('Y-m-d H:i:s');
                if (!in_array($string, $sent)) {
                    $container->get(Email::class)->sendEmailNextSession($booking, $hours, $minutes, 2, $domain);
                    $logger->info('Alert next session: send email to booking id '.$booking->getId() . ' with type ' . $type);
                }
                $sent[] = $string;

            } else if ($type == 0) { // send both for trainer and learner
                // sent email to leaner
                $container->get(Email::class)->sendEmailNextSession($booking, $hours, $minutes, 1, $domain);
                $logger->info('Alert next session: send email to booking id '.$booking->getId() . ' with type ' . $type);

                // sent email to trainer
                $string = $booking->getTrainer()->getId() . "-" . $booking->getModule()->getId() . "" . $booking->getBookingDate()->format('Y-m-d H:i:s');
                if (!in_array($string, $sent)) {
                    $container->get(Email::class)->sendEmailNextSession($booking, $hours, $minutes, 2, $domain);
                    $logger->info('Alert next session: send email to booking id '.$booking->getId() . ' with type ' . $type);
                }
                $sent[] = $string;
            }
        }
    }

    /**
     * @param $container
     * @param $domain
     * @throws Exception
     */
    public function learnerNotStartCourses($container, $domain)
    {
        $logger = $container->get('logger');
        $createdStatus = $this->statusRepository->findOneBy(['appId' => Status::LEARNER_INTERVENTION_CREATED]);
        $learnerInterventions = $this->learnerInterventionRepository->findLearnerNotStartIntervention($createdStatus);
        $logger->info('Not started the journey: total learner interventions ' . count($learnerInterventions));
        $interventionIds = [];
        $durations = [];
        foreach ($learnerInterventions as $learnerIntervention) {
            // calculation the duration of the intervention
            $interventionId = $learnerIntervention->getIntervention()->getId();
            if (!in_array($interventionId, $interventionIds)) {
                $modules = $learnerIntervention->getIntervention()->getModules();
                $hours   = 0;
                $minutes = 0;
                foreach ($modules as $module) {
                    if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                        StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                        StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                        StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                    ) {
                        continue;
                    }
                    $duration = clone $module->getDuration();
                    $hours    += intval($duration->format('H'));
                    $minutes  += intval($duration->format('i'));
                }

                $hours   += floor($minutes / 60);
                $minutes = $minutes % 60;
                $durations[$interventionId] = $hours . ':' . $minutes;
            }
            $container->get(Email::class)->sendEmailLeanerNotStartCourse($learnerIntervention, $durations[$interventionId], $domain);
            $interventionIds[] = $interventionId;
            $logger->info('Not started the journey: send email to learner intervention id '. $learnerIntervention->getId() . ' with learner id ' . $learnerIntervention->getLearner()->getId());
        }
    }

    /**
     * @param $container
     * @param $days
     * @param $domain
     * @throws Exception
     */
    public function learnerNotCompletedCourses($container, $days, $domain)
    {
        $logger = $container->get('logger');
        $finishedStatus = $this->statusRepository->findOneBy(['appId' => Status::LEARNER_INTERVENTION_FINISHED]);
        $learnerInterventions = $this->learnerInterventionRepository->findLearnerNotCompleteIntervention($days, $finishedStatus);
        $logger->info('Not complete the journey: total learner interventions ' . count($learnerInterventions));
        foreach ($learnerInterventions as $learnerIntervention) {
            $container->get(Email::class)->sendEmailLeanerNotCompleteCourse($learnerIntervention, $domain);
            $logger->info('Not started the journey: send email to learner intervention id '. $learnerIntervention->getId() . ' with learner id ' . $learnerIntervention->getLearner()->getId());
        }
    }

    /**
     * @param $container
     * @param $days
     * @param $domain
     * @throws Exception
     */
    public function alertUpdatePlanning($container, $days, $domain)
    {
        $now = new DateTime();
        $now->sub(new \DateInterval('P' . $days . 'D'));
        $profileType = $this->entityManager->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::TRAINER,
            )
        );

        $trainers    = $this->entityManager->getRepository('ApiBundle:Profile')->findBy(['profileType' => $profileType]);

        foreach ($trainers as $trainer) {
            if (!$available = $this->availableCalendarRepository->findAvailabilityByTrainerForAlert($trainer)) {
                continue;
            }

            if ($available->getUpdated() <= $now) {
                $container->get(Email::class)->sendMailPlaningRequest($trainer, $domain);
            }
        }
    }

    /**
     * @param $container
     * @throws Exception
     */
    public function alertReportUpload($container)
    {
        $now               = new DateTime();
        $bookingAgendaDone = $this->bookingAgendaRepository->findBy(
            array('status' => $this->statusRepository->findOneBy(['appId' => Status::WITH_BOOKING_DONE]))
        );

        foreach ($bookingAgendaDone as $done) {
            $report = $this->moduleReportRepository->findOneBy(['module' => $done->getModule()]);

            if (!$report && ($done->getAlertSent() != 1)
                && ($now->diff($done->getUpdated())->days >= 2)
                && ($done->getModule()->getStoredModule()->getAppId() == StoredModule::ONLINE)) {
                $this->sendEmailReport($done->getModule(), $done, $container);
            }
        }
    }

    /**
     * @param $container
     * @param $domain
     */
    public function alertCount($container, $domain)
    {
        $option = [
            'subject'   => $this->translator->trans('admin.alerts.alerts_pending'),
            'profile'   => null,
            'alerts'    => null,
            'domainUrl' => $domain,
            'joinUrl'   => $container->get('router')->generate('admin_dashboard', [], 0),
            'template'  => 'emailAlertPending',
        ];

        // send email to admin for skills, report un validated, replace resources validation
        $doneStatusId = $this->entityManager->getRepository('ApiBundle:Status')
            ->findOneBy(['appId' => Status::REPORT_DONE])->getId();

        $profileType = $this->entityManager->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::ADMIN,
            )
        );

        // count skills trainer waiting validation
        $skillsTrainerWaitingValidation      = $this->entityManager->getRepository('ApiBundle:SkillResourceVarietyProfile')
            ->findBy(['status' => SkillResourceVarietyProfile::STATUS_UNKNOWN]);
        $countSkillsTrainerWaitingValidation = is_array($skillsTrainerWaitingValidation) ? count($skillsTrainerWaitingValidation) : 0;

        $replaceResourcesWaitingValidation      = $this->entityManager->getRepository('ApiBundle:RequestReplacementResource')
            ->findBy(['status' => RequestReplacementResource::STATUS_UNKNOWN]);
        $countReplaceResourcesWaitingValidation = is_array($replaceResourcesWaitingValidation) ? count($replaceResourcesWaitingValidation) : 0;

        $admins = $this->entityManager->getRepository('ApiBundle:Profile')->findBy(['profileType' => $profileType]);
        foreach ($admins as $admin) {
            // count report un validated
            $countReportsUnValidated = $this->entityManager->getRepository('ApiBundle:BookingAgenda')
                ->countAdminReportByStatus($admin->getId(), $doneStatusId);
            $alerts                  = $countSkillsTrainerWaitingValidation + $countReportsUnValidated + $countReplaceResourcesWaitingValidation;

            $container->get(Email::class)->sendMailAlertPending($admin, $domain, $alerts);
        }
    }

    /**
     * @param $module
     * @param $bookingAgenda
     * @param $container
     */
    public function sendEmailReport($module, $bookingAgenda, $container)
    {
        // send email alert to admin/supervisor
        $admins      = [];
        $moduleSends = $this->moduleSendRepository->findBy(['module' => $module]);
        if ($moduleSends) {
            foreach ($moduleSends as $send) {
                $admins[] = $send->getProfile()->getOtherEmail();
                $send->setStatus($this->statusRepository->findOneBy(['appId' => Status::REPORT_DONE]));
                $this->entityManager->persist($send);
                $this->entityManager->flush();
            }
            if ($admins) {
                $message = (new Swift_Message($this->translator->trans('trainers.alerts.reports.reports_not_upload')))
                    ->setFrom(array('team@deepmark.com' => 'Deepmark'))
                    ->setTo($admins)
                    ->setContentType('text/html')
                    ->setBody(
                        $container->get('templating')->render(
                            'ApiBundle:Email:warningUploadReport.html.twig',
                            array(
                                'name'    => $module->getdesignation(),
                                'start'   => $module->getBeginning()->format('Y-m-d H:i:s'),
                                'end'     => $module->getEnding()->format('Y-m-d H:i:s'),
                                'profile' => $bookingAgenda->getLearner(),
                                'joinUrl' => $container->get('router')->generate('trainer_exercices_video_booking_agenda',
                                    array('id' => $module->getId(), 'bid' => $bookingAgenda->getId()), true),
                            )
                        )
                    );

                if ($container->get('mailer')->send($message)) {
                    $bookingAgenda->setAlertSent(true);
                    $this->entityManager->persist($bookingAgenda);
                    $this->entityManager->flush();
                    echo 'Email sent';
                }
            } else {
                echo 'There no email for sending';
            }
        }
        // end send email alert to admin/supervisor
    }
}
