<?php

namespace ApiBundle\Service;

use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\Module;

interface QuizResolverInterface
{
    /** Returns the quiz type suits for that particular resolver */
    public function getResolvedQuizType();

    public function isPublished(Quizes $quiz);

    public function getUrl(Quizes $quiz): string;

    public function getEditUrl(Quizes $quiz): string;

    public function getAnalyzeUrl(Quizes $quiz): string;

    public function wasSurveyTaken(Quizes $quiz, Module $module): bool;

    public function getLastResult(Quizes $quiz, Module $module);
}
