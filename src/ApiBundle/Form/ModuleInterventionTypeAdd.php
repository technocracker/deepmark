<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23/08/18
 * Time: 08:53.
 */

namespace ApiBundle\Form;

use ApiBundle\Entity\ModuleIntervention;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleInterventionTypeAdd extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('quantity', NumberType::class, array(
                'required' => true,
            ))
            ->add('module', ModuleType::class, array(
                'required' => true,
                'label' => false,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ModuleIntervention::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_moduleIntervention';
    }
}
