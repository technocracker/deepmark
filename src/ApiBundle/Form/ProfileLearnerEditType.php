<?php
/**
 * Created by PhpStorm.
 * User: nasri
 * Date: 17/08/2018
 * Time: 14:45.
 */

namespace ApiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileLearnerEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('otherEmail')
            ->remove('companyPost')
            ->add('firstName', TextType::class, array(
                'required' => true,
            ))
            ->add('lastName', TextType::class, array(
                'required' => true,
            ))
            ->add('position', TextType::class, array(
                'required' => false,
            ))
            ->add('fixedPhone', TelType::class, array(
                'required' => false,
            ))
            ->add('mobilePhone', TelType::class, array(
                'required' => false,
            ))
            ->add('entity', EntityType::class, array(
                'class' => 'ApiBundle:Entity',
                'required' => true,
            ))
            ->add('person', PersonPassType::class, array(
                'required' => false,
            ));
    }

    public function getParent()
    {
        return ProfileLiveResourceType::class;
    }
}
