<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;

class RegistrationType extends AbstractType
{
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}
