<?php

namespace ApiBundle\Command;

use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\ProfileVariety;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RemoveDuplicate
 * @package ApiBundle\Command
 */
class RemoveDuplicate extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('remove-duplicate:default')
            ->setDescription('Update default data for profiles');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em    = $this->getContainer()->get('doctrine')->getManager();
        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::LEARNER,
            )
        );

        $learners = $em->getRepository('ApiBundle:Profile')->findBy(array(
            'profileType' => $profileType,
        ));

        $interventions  = $em->getRepository(Intervention::class)->findAll();

        if (empty($learners) || empty($interventions)) {
            return;
        }

        foreach ($learners as $learner) {
            foreach ($interventions as $intervention) {
                $learnerInterventions  = $em->getRepository(LearnerIntervention::class)->findBy(['intervention' => $intervention, 'learner' => $learner]);
                if (count($learnerInterventions) > 1) {
                    foreach ($learnerInterventions as $learnerIntervention) {
                        if ($learnerIntervention->getProgression() == 0) {
                            $em->remove($learnerIntervention);
                        }
                    }
                    $output->writeln('Profile Id:'. $learner->getId(). ' Course Id:' .$intervention->getId() . ' Total:' . count($learnerInterventions));
                }
            }
        }

        $em->flush();
    }
}
