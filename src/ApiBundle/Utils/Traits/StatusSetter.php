<?php

namespace ApiBundle\Utils\Traits;

use ApiBundle\Service\Booking\BookingAgendaStatusService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait StatusSetter
{
    public function setBookingAgendaStatusAction(Request $request)
    {
        $statusId = $request->request->get('status_id');
        $bookingAgendaId = $request->request->get('booking_agenda_id');
        $translator = $this->get('translator');

        if (empty($statusId) || empty($bookingAgendaId)) {
            return new JsonResponse(
                $translator->trans('trainers.http.status_value'),
                Response::HTTP_BAD_REQUEST);
        }

        $statusService = $this->setStatus($bookingAgendaId, $statusId);

        if (false == $statusService->isStatusSet()) {
            return new JsonResponse(
                $translator->trans('trainers.http.status_not_set'),
                Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($translator->trans('trainers.http.status_set_ok'));
    }

    protected function setStatus($bookingAgendaId, $statusId): BookingAgendaStatusService
    {
        $statusService = $this->get('api.service_booking.booking_agenda_status_service');
        $statusService->setStatus((int) $bookingAgendaId, (int) $statusId);

        return $statusService;
    }
}
