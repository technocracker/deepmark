<?php

namespace Traits\Controller;

use Symfony\Component\HttpFoundation\Request;
use TrainerBundle\Exception\BadRequest;

trait HasAjax
{
    /**
     * @param Request $request
     */
    protected function validateIsAjax(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequest('wrong request type');
        }
    }
}