<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 07.03.19
 * Time: 09:36.
 */

namespace AppBundle\Service;

interface LocalisationInterface
{
    public function getTimeZone(): ?string;

    public function setIp(string $ip): void;
}
