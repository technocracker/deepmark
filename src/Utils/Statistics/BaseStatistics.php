<?php


namespace Utils\Statistics;

use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BaseStatistics
 * @package Utils\Statistics
 */
class BaseStatistics
{
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * BaseStatistics constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->token = $tokenStorage;
        $this->em    = $entityManager;
    }

    protected function gUsersByTimeRange($userType, array $conditions = [])
    {
        if (isset($conditions['person'])) {
            $from = isset($conditions['person']['lastLogin'][0]['value'])
                ? new \DateTime($conditions['person']['lastLogin'][0]['value']) : null;
            $to   = isset($conditions['person']['lastLogin'][1]['value'])
                ? new \DateTime($conditions['person']['lastLogin'][1]['value']) : new \DateTime();

            $daily = [];

            if ($from === null && $to === null) {
                return $daily;
            }

            $daysDiff = date_diff($from, $to)->days;

            for ($day = 0; $day <= $daysDiff; $day++) {
                $nDay = clone $from;
                $nDay->modify('+' . $day . ' day');
                $dateStr = $nDay->format('Y-m-d');

                $conditions['person']['lastLogin'][0]['value'] = $dateStr . ' 00:00:00';
                $conditions['person']['lastLogin'][1]['value'] = $dateStr . ' 23:59:59';
                $daily[$dateStr]                               = count(
                    $this->em->getRepository(Profile::class)
                        ->getUsersByConditions($userType, $conditions)
                );
            }

            return $daily;
        }
    }

    /**
     * @param array $conditions
     * @return array
     * @throws \Exception
     */
    public function getLearners(array $conditions = [])
    {
        return [
            'daily' => $this->gUsersByTimeRange(ProfileVariety::LEARNER, $conditions),
            'total' => count($this->em->getRepository(Profile::class)->getUsersByConditions(ProfileVariety::LEARNER, $conditions))
        ];
    }

    /**
     * @param array $conditions
     * @return array
     */
    public function getTrainers(array $conditions = [])
    {
        return [
            'daily' => $this->gUsersByTimeRange(ProfileVariety::TRAINER, $conditions),
            'total' => count($this->em->getRepository(Profile::class)->getUsersByConditions(ProfileVariety::TRAINER, $conditions))
        ];
    }

    /**
     * @param array $conditions
     * @return array
     */
    public function getSupervisors(array $conditions = [])
    {

        return [
            'daily' => $this->gUsersByTimeRange(ProfileVariety::SUPERVISOR, $conditions),
            'total' => count($this->em->getRepository(Profile::class)->getUsersByConditions(ProfileVariety::SUPERVISOR, $conditions))
        ];
    }

    /**
     * @param $times
     * @return string
     */
    function AddPlayTime($times)
    {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours   = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    /**
     * @param $intervention
     * @param $profile
     * @param $doneStatus
     * @return array
     */
    function processInterventionTime($intervention, $profile, $doneStatus)
    {
        $courseModules = $this->em->getRepository('ApiBundle:Module')->findBy(array("intervention" => $intervention->getId()));
        $times = [];
        $totalMinutes = $doneMinutes = 0;
        foreach ($courseModules as $module) {
            if (!$duration = $module->getDuration()) {
                continue;
            }
            $moduleIteration = $this->em->getRepository('ApiBundle:ModuleIteration')->findOneBy(array("module" => $module, 'learner' => $profile, 'status' => $doneStatus));
            if ($moduleIteration) {
                $doneMinutes += intval($duration->format('i')) + (intval($duration->format('H')) * 60);
            }
            $totalMinutes += intval($duration->format('i')) + (intval($duration->format('H')) * 60);
            $times[] = $duration->format('H:i');
        }
        return ['totalMinutes' => $totalMinutes, 'doneMinutes' => $doneMinutes, 'times' => $times];
    }
}
