<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 15.03.19
 * Time: 10:38.
 */

namespace UserBundle\Listener;

use AppBundle\Service\TimeZoneResolver;
use Symfony\Component\EventDispatcher\Event;
use UserBundle\Event\CreateUserBaseEvent;

class UserAddTimeZoneListener
{
    private $timeZoneResolver;

    public function __construct(TimeZoneResolver $timeZoneResolver)
    {
        $this->timeZoneResolver = $timeZoneResolver;
    }

    /**
     * @param CreateUserBaseEvent|Event $event
     *
     * @throws \Exception
     */
    public function onUserRegisterAddTimeZone(Event $event)
    {
        $timeZoneResolver = $this->timeZoneResolver;
        $timeZoneResolver->setUser($event->getUser());

        $timeZoneResolver->getTimeZoneForUser();
        $event->setUser($timeZoneResolver->getUser());
    }
}
