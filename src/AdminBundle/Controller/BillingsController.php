<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\Priority;
use ApiBundle\Entity\Product;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\Quotation;
use ApiBundle\Entity\QuotationLine;
use Controller\AdminControllerInterface;
use Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BillingsController
 * @package AdminBundle\Controller
 */
class BillingsController extends BaseController implements AdminControllerInterface
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('AdminBundle:billings:index.html.twig', array(
            'billings' => [
                'providers' => [],
                'clients'   => [],
            ],
        ));
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function newAction(Request $request)
    {
        $response = new Response();

        $em = $this->getDoctrine()->getManager();

        $data         = $request->request->get('intervention');
        $intervention = new Intervention();

        $customer = $em->getRepository(Profile::class)->find($data['client']);
        $admin    = $em->getRepository(Profile::class)->find($data['client']);

        $intervention->setDesignation($data['designation']);
        $intervention->setCustomer($customer);
        $intervention->setAdmin($admin);
        $intervention->setBeginning(new \DateTime($data['beginning']));
        $intervention->setEnding(new \DateTime($data['ending']));
        $intervention->setPriority($em->getRepository(Priority::class)->findOneBy(array(
                'appId' => 1,
            )
        ));

        $quotation = new Quotation();
        $quotation->setIntervention($intervention);
        $quotation->setAdmin($admin);
        $quotation->setDesignation($data['designation']);

        foreach ($data['modint'] as $modInt) {
            $quotationLine = new QuotationLine();
            $quotationLine->setQuotation($quotation);
            $quotationLine->setQuantity($modInt['quantity']);

            $product = new Product();
            $product->setDesignation($modInt['name']);
            $product->setPrice($modInt['price_total']);
            $quotationLine->setProduct($product);
        }

        $em->persist($intervention);
        $em->flush();

        return $response->setContent('reçu');
    }

    /**
     * @return Response
     */
    public function showAction()
    {
        return (new Response())->setContent('reçu');
    }
}
