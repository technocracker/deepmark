<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\Quizes;
use Controller\BaseController;
use Psr\Log\LoggerInterface;
use Spliced\SurveyMonkey\SurveyMonkeyApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Services\QuizService;
use ApiBundle\Service\QuizServiceAPI;

/**
 * Class QuizController
 * @package AdminBundle\Controller 
 */
class QuizController extends BaseController {

    /**
     * Quiz listing
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, QuizServiceAPI $quizServiceAPI) {
        try {
            $repository = $this->getDoctrine()->getRepository(Quizes::class);
            $quizes = $repository->findBy(['status' => $request->query->get('status', [1])], ['created' => 'DESC']);
            if ($quizes) {
                foreach ($quizes as $key => $quiz) {
                    $quizSteps = [];
                    $quizSteps = $quizServiceAPI->sanitizeQuiz($quiz);
                    $questions_count = 0;
                    $quizes[$key]->steps = $quizSteps;
                    if ($quizSteps) {
                        foreach ($quizSteps as $step) {
                            $questions_count += count($step['questions']);
                        }
                    }
                    $quizes[$key]->questions_count = $questions_count;
                    // Check is played or not
                   /* $isPlayed = $this->getDoctrine()->getRepository(QuizUsersPlayed::class)->findBy(['quiz' => $quiz]);
                    if ($isPlayed) {
                        $quizes[$key]->is_played = true;
                    } else {
                        $quizes[$key]->is_played = false;
                    }*/
                }
            }

            return $this->render('AdminBundle:quiz:index.html.twig', [
                        'quizes' => $quizes,
                        'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession(),
                        'status' => ($request->query->get('status') == '0' ? 'draft' : 'all')
            ]);
        } catch (\Exception $e) {
            /** @var LoggerInterface $logger */
            $logger = $this->get('logger');
            $logger->error($e);
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('notice', 'Could not load the quizzes');

            return $this->render('AdminBundle:quiz:index.html.twig', ['quizes' => $quizes]);
        }
    }

    /**
     * New Quiz
     */
    public function newAction(Quizes $quiz = null, QuizService $quizService, QuizServiceAPI $quizServiceAPI) {
        // Define question types
        $question_types = [
            ['id' => 'qcu', 'name' => 'admin.quiz.create.types.qcu', 'isOnline' => 'online', 'canSurvey' => true],
            ['id' => 'qcm', 'name' => 'admin.quiz.create.types.qcm', 'isOnline' => 'online', 'canSurvey' => true],
            ['id' => 'drag_and_drop', 'name' => 'admin.quiz.create.types.drag_and_drop', 'isOnline' => 'online', 'canSurvey' => true],
            ['id' => 'ordering', 'name' => 'admin.quiz.create.types.ordering', 'isOnline' => 'online', 'canSurvey' => true],
            ['id' => 'category', 'name' => 'admin.quiz.create.types.category', 'isOnline' => 'online', 'canSurvey' => true],
            ['id' => 'text_to_fill', 'name' => 'admin.quiz.create.types.text_to_fill', 'isOnline' => 'online', 'canSurvey' => false],
            ['id' => 'open', 'name' => 'admin.quiz.create.types.open', 'isOnline' => 'online', 'canSurvey' => true],
            ['id' => 'scoring', 'name' => 'admin.quiz.create.types.scoring', 'isOnline' => 'online', 'canSurvey' => true],
        ];

        // EDIT Quiz
        $quizSteps = [];
        // Create a tmp quiz_id
        if ($quiz === null) {
            $quizId = 'tmp_' . uniqid();
        } else {
            $quizId = $quiz->getId();

            $quizSteps = $quizServiceAPI->sanitizeQuiz($quiz);
        }

        // Skills
        $skills = $this->getDoctrine()->getRepository('ApiBundle:Skill')->findAll();

        // Render
        return $this->render('AdminBundle:quiz:new.html.twig', ['question_types' => $question_types, 'quizId' => $quizId, 'quiz' => $quiz, 'steps' => $quizSteps, 'skills' => $skills, 'numSession' => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()]);
    }

    /**
     * Create / Edit Quiz
     * @param Request $request
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request) {
        try {
            $quizValidator = new QuizValidator(Validation::createValidator());
            $quizValidator->setQuizParams($request->request->all());
            $quizValidator->validate();

            /** @var QuizRepository $repository */
            $repository = $this->getDoctrine()->getRepository('ApiBundle:Quizes');
            $quiz = $quizApi->create($request->request->all());

            $quizApi->publish($quiz);
            $repository->save($quiz);

            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('success', 'New survey has been created successfully');
        } catch (SurveyMonkeyApiException $e) {
            /** @var LoggerInterface $logger */
            $logger = $this->get('logger');
            $logger->error($e);
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->clear();
            $flashbag->add('danger', 'Could not create the quiz properly');
        }

        return $this->redirectToRoute('admin_quiz_index');
    }

}
