<?php

namespace AdminBundle\Controller;

use AdminBundle\Services\TrainerBooking\TrainerBookingService;
use ApiBundle\Controller\RestController;
use ApiBundle\Entity\Activities;
use ApiBundle\Entity\AvailableCalendar;
use ApiBundle\Entity\BookingAgenda;
use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\Entity;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Intervention;
use ApiBundle\Entity\InterventionStep;
use ApiBundle\Entity\LearnerGroup;
use ApiBundle\Entity\LearnerIntervention;
use ApiBundle\Entity\Module;
use ApiBundle\Entity\ModuleResponse;
use ApiBundle\Entity\ModuleIteration;
use ApiBundle\Entity\NativeQuizResult;
use ApiBundle\Entity\Organisation;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileDocument;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Entity\ScormSynthesis;
use ApiBundle\Entity\SkillResourceVarietyProfile;
use ApiBundle\Entity\StoredModule;
use ApiBundle\Entity\WorkingHours;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Entity\Person;
use AppBundle\Entity\PersonTimeZone;
use AppBundle\Service\FileApi;
use DateTime;
use Doctrine\ORM\Query\ResultSetMapping;
use Exception;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Traits\Controller\HasUsers;
use Utils\Email;
use Utils\Import\Users;
use Utils\Statistics\KeyNameConst;
use Utils\User;
use Traits\HasAvailableCalendar;
use Traits\Controller\HasWorkingHours;

/**
 * Class LearnersController
 * @package AdminBundle\Controller
 */
class LearnersController extends RestController
{
    use HasUsers;
    use HasAvailableCalendar;
    use HasWorkingHours;
    /**
     * @param Request $request
     * @return Response
     */
    public function groupOrganizationAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $regionalId = $request->request->get('regional_id');
        $regional   = $em->getRepository('ApiBundle:Regional')->find($regionalId);
        $groups     = $em->getRepository('ApiBundle:GroupOrganization')->findBy(array('regional' => $regional));
        $result     = [];
        foreach ($groups as $group) {
            $result[] = array('id' => $group->getId(), 'designation' => $group->getDesignation());
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @return mixed
     */
    private function getInterventions()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('ApiBundle:Intervention', 'i');
        $rsm->addFieldResult('i', 'id', 'id');
        $rsm->addFieldResult('i', 'designation', 'designation');
        $rsm->addFieldResult('i', 'admin', 'admin_id');
        $rsm->addFieldResult('i', 'customer', 'customer_id');
        $rsm->addFieldResult('i', 'comment', 'comment');
        $rsm->addFieldResult('i', 'status', 'status_id');

        return $this->getDoctrine()->getManager()
            ->createNativeQuery(
                'select i.* from intervention i LEFT join profile p on i.admin_id = p.id  where p.entity_id = ?',
                $rsm
            )
            ->setParameter(1, $this->getEntityFromProfile()->getId())
            ->getResult();
    }

    /**
     * Lists all profile entities.
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function indexAction(Request $request)
    {
        $em                    = $this->getDoctrine()->getManager();
        // prepare the search parameters
        $searchParam['organization'] = [];
        $searchParam['groups'] = $em->getRepository('ApiBundle:Group')->findAll();

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            $searchParam['organization'] = $em->getRepository('ApiBundle:Organisation')->findAll();
        } else {
            $organization = $this->getEntityFromProfile()->getOrganisation();
            $searchParam['organization'][] = $organization;
        }

        return $this->render(
            'AdminBundle:learners:index.html.twig',
            [
                'interventions' => $this->getInterventions(),
                'searchParam'   => $searchParam,
                'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
            ]
        );
    }

    public function getEntityByLearnerAction(Request $request)
    {
        $entityId = $request->request->get('entity_id');
        $entity = $this->getDoctrine()->getRepository(Entity::class)
            ->find($entityId);
        $response = new Response(json_encode( array('street' => $entity->getStreet(), 'expansionStreet' => $entity->getExpansionStreet(), 'postalCode' => $entity->getPostalCode(), 'city' => $entity->getCity(), 'country' => $entity->getCountry()) ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getCourseLearnersBySearchAction(Request $request)
    {
        $interventionId = $request->get('interventionId');
        $intervention = $this->getDoctrine()->getRepository(Intervention::class)->findOneBy(['id' => $interventionId]);
        $interventionLearners = $intervention->getLearners();
        $learnerIds = [];
        foreach ($interventionLearners as $learner){
            $learnerIds[] = $learner->getLearner()->getPerson()->getId();
        }

        $learners = [];
        $em                    = $this->getDoctrine()->getManager();
        $options = $request->query->all();
        $organizationId = $request->get('organizationId');
        $organisation = $this->getDoctrine()->getRepository(Organisation::class)
            ->findOneBy([KeyNameConst::KEY_ID => $organizationId]);
        $options[KeyNameConst::KEY_ORGANISATION] = $organisation;

        if(isset($learnerIds)){
            $options['courseFilter'] = $learnerIds;
        }

        $listLearner = $em->getRepository('ApiBundle:Profile')->findLearnerByCondition($options);

        foreach ($listLearner as $learner) {
            $groupNames    = [];
            $learnerGroups = $em->getRepository('ApiBundle:LearnerGroup')->findBy(array("learner" => $learner->getId()));
            if ($learnerGroups) {
                foreach ($learnerGroups as $learnerGroup) {
                    $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                }
            }
            $groupName  = implode(", ", $groupNames);
            $learners[] = array("learner" => $learner, "groupName" => $groupName);
        }
        $view = $this->renderView('AdminBundle:learners:course_learners_table.html.twig', ['learners' => $learners]);
        return new JsonResponse($view);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getLearnersBySearchAction(Request $request)
    {
        $learners = [];
        $em                    = $this->getDoctrine()->getManager();
        $options = $request->query->all();
        $creationDate = $request->get('creationDate') ? (new \DateTime($request->get('creationDate'))) : (new \DateTime('first day of this month'));
        $connectionDate   = $request->get('connectionDate') ? (new \DateTime($request->get('connectionDate'))) : new \DateTime('last day of this month');
        $data = [];
        $organizationId = $request->get('organizationId');
        $organisation = $this->getDoctrine()->getRepository(Organisation::class)
            ->findOneBy([KeyNameConst::KEY_ID => $organizationId]);
        $options[KeyNameConst::KEY_ORGANISATION] = $organisation;

        $listLearner = $em->getRepository('ApiBundle:Profile')->findLearnerByCondition($options);

        foreach ($listLearner as $learner) {
            $groupNames    = [];
            $learnerGroups = $em->getRepository('ApiBundle:LearnerGroup')->findBy(array("learner" => $learner->getId()));
            if ($learnerGroups) {
                foreach ($learnerGroups as $learnerGroup) {
                    $groupNames[] = $learnerGroup->getGroup()->getDesignation();
                }
            }
            $groupName  = implode(", ", $groupNames);
            $learners[] = array("learner" => $learner, "groupName" => $groupName);
        }

        $templateName = "learners_table";
        if($request->get("templateName")) {
            $templateName = $request->get("templateName");
        }
        $view = $this->renderView("AdminBundle:learners:$templateName.html.twig", ['learners' => $learners]);
        return new JsonResponse($view);
    }

    /**
     * Creates a new profile entity.
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities        = $em->getRepository('ApiBundle:Entity')->findAll();
        return $this->render('AdminBundle:learners:formLearners.html.twig', array(
            'regionals' => $em->getRepository('ApiBundle:Regional')->findAll(),
            'timeZones' => $em->getRepository(PersonTimeZone::class)->findAll(),
            'entities'    => $entities,
            'currentProfile'  => $this->getCurrentProfile(),
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function uploadAction(Request $request)
    {
        $profile = $this->getDoctrine()->getRepository('ApiBundle:Profile')->findOneBy(array(
            'person' => $this->getUser()->getId(),
        ));

        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if (!$form->isSubmitted() || !$form->isValid()) {
            $fields = array_merge($this->get(Users::class)->getFields(),
                [
                    'regional'  => false,
                    'group_org' => false,
                    'workplace' => false,
                ]);
            $fieldSorted = array_replace(
                array_flip(array('employeeId', 'lastName', 'firstName', 'position', 'email', 'fixedPhone', 'mobilePhone', 'managerEmail', 'otherEmail', 'group', 'organization', 'entity', 'regional', 'group_org', 'workplace')),
                $fields);
            return $this->render('AdminBundle:learners:upload.html.twig', array(
                'headers'      => [],
                // Import fields
                'requirements' => [
                    'post_max_size'       => ini_get('post_max_size'),
                    'upload_max_filesize' => ini_get('upload_max_filesize'),
                ],
                'importType'   => 'learners',
                'fields'       => $fieldSorted,
            ));
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('file_descriptor')['path'];
        $fileDescriptor->setName($file->getFilename());
        $fileDescriptor->setDirectory($file->getPath());
        $fileDescriptor->setMimeType($file->getMimeType());
        $fileDescriptor->setSize($file->getSize());

        $fileDescriptor->getPath();
        $fileDescriptor = $fileApi->uploadPrivateFile($file);
        $em             = $this->getDoctrine()->getManager();

        $em->persist($fileDescriptor);
        $document = new EducationalDocument();
        $document->setName($fileDescriptor->getName());
        $document->setFileDescriptor($fileDescriptor);
        $document->setProfile($profile);

        $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

        switch ($mimeType) {
            case 'audio':
                $appId = EducationalDocVariety::AUDIO;
                break;
            case 'video':
                $appId = EducationalDocVariety::VIDEO;
                break;
            case 'application':
                $relativePath = $fileDescriptor->getDirectory() . '/' . $fileDescriptor->getPath();
                $response     = $this->forward('ImportBundle:LearnerImport:importUser', [
                    'path' => $relativePath,
                ]);

                $response = json_decode($response->getContent());

                if (!empty($response->errors)) {
                    $flashbag = $this->get('session')->getFlashBag();
                    $flashbag->add('error', $response->message);

                    return $this->render('AdminBundle:clients:upload.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }
                $flashbag = $this->get('session')->getFlashBag();
                $flashbag->add('notice', 'Import successfully completed');

                return $this->redirect($this->generateUrl('admin_learners_import', ['role' => 'learner']));
                break;
            default:
                $appId = EducationalDocVariety::DOCUMENT;
                break;
        }

        $document->setEducationalDocVariety(
            $this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')
                ->findOneBy(array('appId' => $appId))
        );

        $em->persist($document);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_clients_index'));
    }

    /**
     * Process ajax upload import file
     * @param Request $request
     * @return JsonResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function ajaxUploadAction(Request $request)
    {
        $type = $request->get('task');

        switch ($type) {
            case 'uploadFile':
                /**
                 * @var UploadedFile $file
                 */
                $file = $request->files->get('file');

                if ($file->getError() !== 0) {
                    return $this->json([
                        'error' => $file->getError(),
                        'msg'   => 'File error',
                    ]);
                }

                /**
                 * @var Users $importer
                 */
                $importer  = $this->get(Users::class)->loadFile($file->getPathname(), $request->get('delimiter'));
                $sign      = md5_file($file->getPathname()) . $this->getUser()->getId();
                $uploadDir = $this->get('kernel')->getRootDir() . '/../var/uploads';

                if (!file_exists($uploadDir) || !is_dir($uploadDir)) {
                    mkdir($uploadDir);
                }

                move_uploaded_file($file->getPathname(), $uploadDir . '/' . $sign);

                /**
                 * Return parsed header from import file
                 */
                return $this->json([
                    'error' => $file->getError(),
                    'msg'   => '',
                    'data'  => [
                        'headers' => $importer->getHeaders(),
                        'sign'    => $sign,
                    ],
                ]);

                break;
            case 'importUsers':
                $uploadedDirFile = $this->get('kernel')->getRootDir() . '/../var/uploads/' . $request->get('sign');

                if (!file_exists($uploadedDirFile)) {
                    return $this->json([
                        'error' => true,
                        'msg'   => 'File not found',
                    ]);
                }
                /**
                 * @var Users $importer
                 */
                $importer = $this->get(Users::class)->loadFile(
                    $uploadedDirFile,
                    $request->get('delimiter')
                );

                /**
                 * Temporary code for importing
                 */
                $importer->setMappedFields($request->get('mappedFields'));
                $importer->process(ProfileVariety::LEARNER);

                unlink($uploadedDirFile);

                return $this->json([
                    'redirect' => $this->generateUrl('admin_learners_index'),
                    'console'  => $importer->getErrors(),
                ]);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function saveAction(Request $request)
    {
        $profileEntity = new Profile();

        $em            = $this->getDoctrine()->getManager();
        $lastName      = $request->request->get('lastName');
        $firstName     = $request->request->get('firstName');
        $position      = $request->request->get('position');
        $fixedPhone    = $request->request->get('fixedPhone');
        $mobilePhone   = $request->request->get('mobilePhone');
        $email         = $request->request->get('email');
        $manager_email = $request->request->get('manager_email');
        $otherEmail    = $request->request->get('otherEmail');
        $employee_id   = $request->request->get('employee_id');
        $password      = $request->request->get('password');
        if (empty($password)) $password = Profile::DEFAULT_PASSWORD;
        // $rpPassword    = $request->request->get('rpPassword');
        $status        = $request->request->get('status');
        $idGroup       = $request->request->get('group');
        $idRegional    = $request->request->get('regional');
        $timeZoneId    = $request->request->get('time_zone_id');
        $checkEmail    = $this->getDoctrine()->getRepository(Person::class)->findOneBy(['email' => $email]);

        if (!$checkEmail) {
            // avoid save the profile with null entity
            //if (!$this->getUser()->isRole('ROLE_SUPER_ADMIN')) {
                $profileEntity->setEntity($this->getEntityFromProfile());
            //}

            $profileEntity->setProfileType($em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
                array(
                    'appId' => ProfileVariety::LEARNER,
                )
            ));
            $timeZone = $this->getDoctrine()->getRepository('AppBundle:PersonTimeZone')
                ->find($timeZoneId);

            if ($idRegional) {
                $profileEntity->setRegional($this->getDoctrine()->getRepository('ApiBundle:Regional')
                    ->find($idRegional));
            }

            $person = new Person();
            $person->setEmail($email);
            $person->setTimeZone($timeZone);
            $person->setUsername($email);
            $person->setEnabled($status);
            $person->setPlainPassword($password);
            $em->persist($person);

            $profileEntity->setPerson($person);
            $profileEntity->getPerson()->setRoles(array('ROLE_LEARNER'));
            $profileEntity->setCreationDate(new DateTime());
            $profileEntity->setUpdateDate(new DateTime());
            $profileEntity->setBillingCode('');
            $profileEntity->setStreet('');
            $profileEntity->setLastName($lastName);
            $profileEntity->setFirstName($firstName);
            $profileEntity->setPosition($position);
            $profileEntity->setFixedPhone($fixedPhone);
            $profileEntity->setMobilePhone($mobilePhone);
            $profileEntity->setManagerEmail($manager_email);
            $profileEntity->setOtherEmail($otherEmail);
            $profileEntity->setEmployeeId($employee_id);
            $data    = $request->request->all();
            $profileEntity->setWorkingHours($this->daysProcessing($data));

            if ($idGroup) {
                $group = $this->getDoctrine()->getRepository('ApiBundle:GroupOrganization')
                    ->find($idGroup);
                $profileEntity->setGroupOrganization($group);
            }

            if ($request->request->get('lunch_start_time') && $request->request->get('lunch_end_time')) {
                $now            = new DateTime('now');
                $lunchStartTime = explode(":", $request->request->get('lunch_start_time'));
                $lunchEndTime   = explode(":", $request->request->get('lunch_end_time'));
                $profileEntity->setLunchStartTime(clone $now->setTime($lunchStartTime[0], $lunchStartTime[1]));
                $profileEntity->setLunchEndTime(clone $now->setTime($lunchEndTime[0], $lunchEndTime[1]));
            }

            $em->persist($profileEntity);

            // Setup reset token
            $token        = $this->container->get('fos_user.util.token_generator');
            $personEntity = $profileEntity->getPerson();
            $personEntity->setPasswordRequestedAt(new \DateTime());
            $personEntity->setConfirmationToken($token->generateToken());

            $em->persist($personEntity);

            $em->flush();
            $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/profile/';
            if (isset($_FILES['avatarFile'])) {
                $this->uploadProfileInfo($profileEntity, $targetDir, $_FILES['avatarFile']);
            }

            $this->get(Email::class)->sendMailNewUserRegistration($profileEntity);

            return $this->redirectToRoute('admin_learners_show', array('id' => $profileEntity->getId()));
        }

        $this->addFlash('error', 'This email already exists');

        return $this->redirectToRoute('admin_learners_new');
    }

    /**
     * Finds and displays a profile entity.
     *
     * @param Profile $profile
     *
     * @return Response
     */
    public function showAction(Profile $profile)
    {
        $data          = [];
        $interventions = $this->getDoctrine()->getRepository('ApiBundle:LearnerIntervention')
            ->findBy(['learner' => $profile]);
        if ($interventions) {
            foreach ($interventions as $intervention) {
                $modules = $intervention->getIntervention()->getModules();
                if ($modules) {
                    foreach ($modules as $module) {
                        if (StoredModule::CONCEPTION === $module->getStoredModule()->getAppId() ||
                            StoredModule::PILOTAGE === $module->getStoredModule()->getAppId() ||
                            StoredModule::TEAM_PREPARATION === $module->getStoredModule()->getAppId()||
                            StoredModule::ASSESSMENT === $module->getStoredModule()->getAppId()
                        ) {
                            continue;
                        }
                        $courseName           = $module->getIntervention()->getDesignation();
                        $moduleId             = $module->getId();
                        $moduleName           = $module->getDesignation();
                        $moduleType           = $module->getStoredModule()->getDesignation();
                        $moduleEnd            = $module->getEnding();
                        $moduleDuration       = $module->getDuration();
                        $realtime             = 'N/A';
                        $status               = 'Unknown';
                        $targetEvaluation     = 'N/A';
                        $trainer              = 'N/A';
                        $grade                = 'N/A';
                        $moduleTrainerRating  = 'N/A';
                        $moduleTrainerComment = 'N/A';
                        $isBook               = false;

                        if ($module->getStoredModule()->getAppId() == StoredModule::QUIZ) {
                            $targetEvaluation = $module->getScoreQuiz() ? $module->getScoreQuiz() : $targetEvaluation;
                        }

                        if (in_array($module->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
                            $isBook                = true;
                            $bookingAgendaByModule = $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')->findOneBy(array(
                                'module'  => $module,
                                'learner' => $profile,
                            ));
                            if ($bookingAgendaByModule) {
                                $grade   = $bookingAgendaByModule->getLearnerGrade();
                                $trainer = $bookingAgendaByModule->getTrainer()->getPerson()->getEmail();

                                $moduleTrainerResponse = $rankingGrades = $this->getDoctrine()->getRepository('ApiBundle:ModuleTrainerResponse')
                                    ->findOneBy(array(
                                        'trainer' => $bookingAgendaByModule->getTrainer()->getPerson(),
                                        'module'  => $module,
                                    ));
                                $moduleTrainerRating   = $moduleTrainerResponse != null ? $moduleTrainerResponse->getNotation() : $moduleTrainerRating;
                                $moduleTrainerComment  = $moduleTrainerResponse != null ? $moduleTrainerResponse->getDesignation() : $moduleTrainerComment;
                            }
                        } elseif ($module->getQuiz()) {
                            //todo: will be implemented new quiz here
                            $grade = '';
                            /*if ('NATIVE' === $module->getQuiz()->getType()) {
                                $results = $this->getDoctrine()->getRepository(NativeQuizResult::class)
                                    ->findBy([
                                        'student' => $profile,
                                        'quiz'    => $module->getQuiz(),
                                    ]);
                                if ($results) {
                                    $grade = '';
                                    foreach ($results as $k => $result) {
                                        if ($k < count($results) && $k > 0) {
                                            $grade .= ' | ';
                                        }
                                        $grade .= $result ? round(($result->getScore() / $module->getQuiz()->getTotalPoints()) * 100,
                                            2) : 0;
                                        $grade .= ' %';
                                    }
                                }
                            }*/
                        }

                        $iteration = $this->getDoctrine()->getRepository('ApiBundle:ModuleIteration')
                            ->findOneBy(array('learner' => $profile, 'module' => $module));
                        $realtime  = $iteration != null && $iteration->getLearnedTime() != null ? $iteration->getLearnedTime() : $realtime;
                        $status    = $iteration != null && $iteration->getStatus() != null ? $iteration->getStatus()->getDesignation() : $status;

                        $moduleResponse = $this->getDoctrine()
                            ->getRepository('ApiBundle:ModuleResponse')
                            ->findOneBy(['module' => $module, 'learner' => $profile]);
                        $moduleRating   = $moduleResponse != null ? $moduleResponse->getNotation() : 'N/A';
                        $moduleComment  = $moduleResponse != null ? $moduleResponse->getDesignation() : 'N/A';

                        $data[] = array(
                            'isBook'               => $isBook,
                            'courseName'           => $courseName,
                            'moduleId'             => $moduleId,
                            'moduleName'           => $moduleName,
                            'moduleType'           => $moduleType,
                            'status'               => $status,
                            'moduleEnd'            => $moduleEnd,
                            'moduleDuration'       => $moduleDuration,
                            'realtime'             => $realtime,
                            'trainer'              => $trainer,
                            'targetEvaluation'     => $targetEvaluation,
                            'score'                => $grade,
                            'moduleRating'         => $moduleRating,
                            'moduleComment'        => $moduleComment,
                            'moduleTrainerRating'  => $moduleTrainerRating,
                            'moduleTrainerComment' => $moduleTrainerComment,
                        );

                    }
                }
            }
        }


        return $this->render('AdminBundle:learners:show.html.twig', array(
            'data'          => $data,
            'profile'       => $profile,
            'interventions' => $interventions,
            'booking'       => $this->getDoctrine()->getRepository('ApiBundle:BookingAgenda')
                ->findBy(['learner' => $profile]),
        ));
    }

    public function moduleAction($profileId, $id, Request $request)
    {
        /** @var Module $moduleToDo */
        $moduleToDo = $this->getDoctrine()
            ->getRepository('ApiBundle:Module')
            ->find($id);
        $profile    = $this->getDoctrine()
            ->getRepository('ApiBundle:Profile')
            ->find($profileId);

        $intervention = $moduleToDo->getIntervention();

        $bookingModule = null;
        $isBooked      = false;
        if (in_array($moduleToDo->getStoredModule()->getAppId(), StoredModule::MODULE_REQUIRE_BOOKING)) {
            $bookingModule = $this->getDoctrine()
                ->getRepository('ApiBundle:BookingAgenda')
                ->findOneBy(array(
                    'learner' => $profile,
                    'module'  => $moduleToDo,
                ));

            if ($bookingModule) {
                $isBooked = true;
            }
        }

        return $this->render('AdminBundle:learners:module.html.twig', array(
            'intervention' => $intervention,
            'module'       => $moduleToDo,
            'isBooked'     => $isBooked,
            'booked'       => $bookingModule,
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     * @param Request $request
     * @param Profile $profile
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function editAction(Request $request, Profile $profile)
    {
        $em              = $this->getDoctrine()->getManager();
        $entities        = $em->getRepository('ApiBundle:Entity')->findBy(['organisation' => $profile->getEntity()->getOrganisation()->getId()]);
        $timeZones       = $em->getRepository(PersonTimeZone::class)->findAll();
        $regionals       = $em->getRepository('ApiBundle:Regional')->findAll();
        $person          = $profile->getPerson();
        $learnerGroups   = $em->getRepository('ApiBundle:LearnerGroup')
            ->findBy(['learner' => $profile]);
        $learnerGroupIds = [];
        foreach ($learnerGroups as $group) {
            if ($group->getGroup()) {
                $learnerGroupIds[] = $group->getGroup()->getId();
            }
        }

        return $this->render('AdminBundle:learners:formLearners.html.twig', array(
            'profile'         => $profile,
            'entities'        => $entities,
            'learnerGroupIds' => $learnerGroupIds,
            'person'          => $person,
            'timeZones'       => $timeZones,
            'regionals'       => $regionals,
            'isEdit'          => true,
            'numSession'    => $this->getDoctrine()->getRepository(BookingAgenda::class)->getNumberCurrentSession()
        ));
    }

    public function deleteFileAction(Request $request)
    {
        $profileId = $request->request->get('profileId');
        $em        = $this->getDoctrine()->getManager();
        $targetDir = $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath();
        $profile   = $document = $this->getDoctrine()->getRepository('ApiBundle:Profile')->find($profileId);
        if (file_exists($targetDir . $profile->getAvatar())) {
            unlink($targetDir . $profile->getAvatar());
            $profile->setAvatar(null);
        }
        $em->flush();

        return $this->redirectToRoute('admin_learners_edit', array('id' => $profileId));
    }

    public function uploadProfileInfo(Profile $profile, $targetDir, $file)
    {
        $array      = explode('.', $file['name']);
        $extension  = end($array);
        $fileName   = sha1(uniqid(mt_rand(), true)) . '.' . $extension;
        $targetFile = $targetDir . $fileName;
        $path       = '/files/profile/' . $fileName;

        if (move_uploaded_file($file['tmp_name'], $targetFile)) {
            $em = $this->getDoctrine()->getManager();
            $profile->setAvatar($path);
            $em->persist($profile);
            $em->flush();

        }

        return $fileName;
    }

    public function saveEditAction(Request $request, Profile $profile)
    {
        $person        = $profile->getPerson();
        $entityManager = $this->getDoctrine()->getManager();
        $lastName      = $request->request->get('lastName');
        $firstName     = $request->request->get('firstName');
        $position      = $request->request->get('position');
        $fixedPhone    = $request->request->get('fixedPhone');
        $mobilePhone   = $request->request->get('mobilePhone');
        $email         = $request->request->get('email');
        $manager_email = $request->request->get('manager_email');
        $otherEmail    = $request->request->get('otherEmail');
        $employee_id   = $request->request->get('employee_id');
        $password      = $request->request->get('password');
        $status        = $request->request->get('status');
        $idEntity      = $request->request->get('entity');
        $idRegional    = $request->request->get('regional');
        $idWorkplace   = $request->request->get('workplace');
        $idGroup       = $request->request->get('group');
        $timeZoneId    = $request->request->get('time_zone_id');
        $checkEmail = $this->getDoctrine()->getRepository(Person::class)->findOneBy(['email' => $email]);

        if (trim($email) == trim($person->getEmail()) || !$checkEmail) {
            $timeZone = $this->getDoctrine()->getRepository('AppBundle:PersonTimeZone')
                ->find($timeZoneId);
            $person->setEmail($email);
            $person->setEnabled($status);
            $person->setPlainPassword($password);
            $person->setTimeZone($timeZone);
            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updatePassword($person);
            $entityManager->persist($person);
        } else {
            $flashbag = $this->get('session')->getFlashBag();
            $flashbag->add('error', 'This email already exists');

            return $this->redirectToRoute('admin_learners_edit', array('id' => $profile->getId()));
        }


        $data    = $request->request->all();
        $profile->setWorkingHours($this->daysProcessing($data));

        $profile->setLastName($lastName);
        $profile->setFirstName($firstName);
        $profile->setPosition($position);
        $profile->setFixedPhone($fixedPhone);
        $profile->setMobilePhone($mobilePhone);
        $profile->setManagerEmail($manager_email);
        $profile->setOtherEmail($otherEmail);
        $profile->setEmployeeId($employee_id);

        $profile->setUpdateDate(new DateTime());
        if ($request->request->get('lunch_start_time') && $request->request->get('lunch_end_time')) {
            $now            = new DateTime('now');
            $lunchStartTime = explode(":", $request->request->get('lunch_start_time'));
            $lunchEndTime   = explode(":", $request->request->get('lunch_end_time'));
            $profile->setLunchStartTime(clone $now->setTime($lunchStartTime[0], $lunchStartTime[1]));
            $profile->setLunchEndTime(clone $now->setTime($lunchEndTime[0], $lunchEndTime[1]));
        }

        if ($idEntity) {
            $entity = $this->getDoctrine()->getRepository('ApiBundle:Entity')
                ->find($idEntity);
            $profile->setEntity($entity);
            $profile->setRegional(null);
            $profile->setGroupOrganization(null);
            $profile->setWorkplace(null);
            if ($idRegional) {
                $regional = $this->getDoctrine()->getRepository('ApiBundle:Regional')
                    ->find($idRegional);
                $profile->setRegional($regional);
                if ($idGroup) {
                    $group = $this->getDoctrine()->getRepository('ApiBundle:GroupOrganization')
                        ->find($idGroup);
                    $profile->setGroupOrganization($group);
                    if ($idWorkplace) {
                        $workplace = $this->getDoctrine()->getRepository('ApiBundle:Workplace')
                            ->find($idWorkplace);
                        $profile->setWorkplace($workplace);
                    }
                }
            }
        }

        $entityManager->persist($profile);
        $entityManager->flush();
        $targetDir = $this->get('kernel')->getRootDir() . '/../web/files/profile/';
        if (isset($_FILES['avatarFile'])) {
            $this->uploadProfileInfo($profile, $targetDir, $_FILES['avatarFile']);
        }

        $flashbag = $this->get('session')->getFlashBag();
        $flashbag->add('success', 'All changed successfully');

        return $this->redirectToRoute('admin_learners_edit', array('id' => $profile->getId()));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteLearnersAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $type       = $request->request->get('type');
        $learnerIds = $request->request->get('selectedProfiles');

        foreach ($learnerIds as $id){
            $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
            $person     = $profile->getPerson();
            if ($type == 1) {
                $this->deleteLearner($person);
            } else {
                $person->setEnabled(0);
                $em->persist($person);
                $em->flush();
            }
        }
        return $this->redirectToRoute('admin_learners_index');
    }

    /**
     * Deletes a profile entity.
     *
     * @param Request $request
     * @param $id
     * @param Profile $profile
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em         = $this->getDoctrine()->getManager();
        $isDelete   = $request->request->get('isDelete');

        $profile    = $em->getRepository('ApiBundle:Profile')->find($id);
        $person     = $profile->getPerson();

        if ($isDelete) {
            $this->deleteLearner($person);
        } else {
            $person->setEnabled(0);
            $em->persist($person);
            $em->flush();
        }

        return $this->redirectToRoute('admin_learners_index');
    }

    /**
     * Delete a learner.
     *
     * @param Profile $profile The profile entity
     *
     * @return Boolean
     */
    private function deleteLearner(Person $user)
    {
        $em      = $this->getDoctrine()->getManager();
        $profile               = $em->getRepository(Profile::class)->findOneBy(['person' => $user]);
        $learnerInterventions  = $em->getRepository(LearnerIntervention::class)->findBy(['learner' => $profile]);
        $workHours             = $em->getRepository(WorkingHours::class)->findBy(['profile' => $profile]);
        $calendars             = $em->getRepository(AvailableCalendar::class)->findBy(['profile' => $profile]);
        $learnerGroups         = $em->getRepository(LearnerGroup::class)->findBy(['learner' => $profile]);
        $learnerVerieties      = $em->getRepository(SkillResourceVarietyProfile::class)->findBy(['profile' => $profile]);
        $learnerDocuments      = $em->getRepository(ProfileDocument::class)->findBy(['profile' => $profile]);
        $learnerScormSynthesis = $em->getRepository(ScormSynthesis::class)->findBy(['profile' => $profile]);
        $learnerActivities     = $em->getRepository(Activities::class)->findBy(['profile' => $profile]);
        $learnerRespones       = $em->getRepository(ModuleResponse::class)->findBy(['learner' => $profile]);
        $moduleIterations      = $em->getRepository(ModuleIteration::class)->findBy(['learner' => $profile]);
        $bookingAgendas         = $em->getRepository(BookingAgenda::class)->findBy(['learner' => $profile]);

        foreach ($learnerInterventions as $learnerIntervention) {
            if ($learnerIntervention) {
                $em->remove($learnerIntervention);
            }
        }

        foreach ($workHours as $workHour) {
            if ($workHour) {
                $em->remove($workHour);
            }
        }

        foreach ($calendars as $calendar) {
            if ($calendar) {
                $em->remove($calendar);
            }
        }

        foreach ($bookingAgendas as $bookingAgenda) {
            if ($bookingAgenda) {
                $em->remove($bookingAgenda);
            }
        }

        foreach ($learnerGroups as $learnerGroup) {
            if ($learnerGroup) {
                $em->remove($learnerGroup);
            }
        }

        foreach ($learnerVerieties as $learnerVeriety) {
            if ($learnerVeriety) {
                $em->remove($learnerVeriety);
            }
        }

        foreach ($learnerDocuments as $learnerDocument) {
            if ($learnerDocument) {
                $em->remove($learnerDocument);
            }
        }

        foreach ($learnerScormSynthesis as $learnerScormSynthesi) {
            if ($learnerScormSynthesi) {
                $em->remove($learnerScormSynthesi);
            }
        }

        foreach ($learnerActivities as $learnerActivity) {
            if ($learnerActivity) {
                $em->remove($learnerActivity);
            }
        }

        foreach ($learnerRespones as $learnerRespone) {
            if ($learnerRespone) {
                $em->remove($learnerRespone);
            }
        }

        foreach ($moduleIterations as $moduleIteration) {
            if ($moduleIteration) {
                $em->remove($moduleIteration);
            }
        }

        $em->remove($user);
        $em->remove($profile);
        $em->flush();
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return Form The form
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_learners_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function testAction(TrainerBookingService $trainerBookingService)
    {
        $stepRepo = $this->getDoctrine()->getRepository(InterventionStep::class);
        $trainerBookingService->automaticAssignTrainers($stepRepo->find(58));
    }
}
