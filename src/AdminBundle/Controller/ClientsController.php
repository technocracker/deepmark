<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\EducationalDocument;
use ApiBundle\Entity\EducationalDocVariety;
use ApiBundle\Entity\FileDescriptor;
use ApiBundle\Entity\Profile;
use ApiBundle\Entity\ProfileVariety;
use ApiBundle\Form\FileDescriptorType;
use AppBundle\Service\FileApi;
use Controller\BaseController;
use DateTime;
use Exception;
use ImportBundle\Entity\ImportUser;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ClientsController
 * @package AdminBundle\Controller
 */
class ClientsController extends BaseController
{
    /**
     * Lists all profile entities.
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $profileType = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
            array(
                'appId' => ProfileVariety::CLIENT,
            )
        );
        $clients     = $em->getRepository('ApiBundle:Profile')->findBy(array(
            'profileType' => $profileType,
        ));

        $interventions = $this->getDoctrine()->getRepository('ApiBundle:Intervention')->findAll();

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $clients   = $paginator->paginate($clients, $request->query->getInt('page', 1));

        return $this->render('AdminBundle:clients:index.html.twig', array(
            'clients'             => $clients,
            'interventionsClient' => $interventions,
        ));
    }

    /**
     * Creates a new profile entity.
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function newAction(Request $request)
    {
        $profile = new Profile();
        $form    = $this->createForm('ApiBundle\Form\ProfileClientType', $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailProfile = $profile->getPerson()->getEmail();
            $em           = $this->getDoctrine()->getManager();
            $profileType  = $em->getRepository('ApiBundle:ProfileVariety')->findOneBy(
                array(
                    'appId' => ProfileVariety::CLIENT,
                )
            );
            $profile->setProfileType($profileType);
            $profile->getPerson()->setRoles(array('ROLE_CLIENT'));
            $profile->getPerson()->setUsername($emailProfile);
            $profile->getPerson()->setEnabled(true);
            $profile->setCreationDate(new DateTime());
            $profile->setOtherEmail($emailProfile);
            $profile->setUpdateDate(new DateTime());
            $profile->setBillingCode('');
            $profile->setStreet('');

            $em->persist($profile->getPerson());
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('admin_clients_show', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:clients:formClients.html.twig', array(
            'profile' => $profile,
            'form'    => $form->createView(),
        ));
    }

    /**
     * Upload profile entity.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function uploadAction(Request $request)
    {
        $profile = $this->getCurrentProfile();

        $fileDescriptor = new FileDescriptor();
        $form           = $this->createForm(FileDescriptorType::class, $fileDescriptor);
        $form->handleRequest($request);
        $fileApi = $this->get(FileApi::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $file           = $fileDescriptor->getPath();
            $fileDescriptor = $fileApi->uploadPrivateFile($file);

            $em = $this->getDoctrine()->getManager();

            $em->persist($fileDescriptor);
            $document = new EducationalDocument();
            echo $fileDescriptor->getName();
            $document->setName($fileDescriptor->getName());
            $document->setFileDescriptor($fileDescriptor);
            $document->setProfile($profile);

            $mimeType = explode('/', $fileDescriptor->getMimeType())[0];

            switch ($mimeType) {
                case 'audio':
                    $appId = EducationalDocVariety::AUDIO;
                    break;
                case 'video':
                    $appId = EducationalDocVariety::VIDEO;
                    break;
                default:
                    $appId = EducationalDocVariety::DOCUMENT;
                    break;
            }
            $document->setEducationalDocVariety($this->getDoctrine()->getRepository('ApiBundle:EducationalDocVariety')->findOneBy(array(
                    'appId' => $appId,
                )
            ));
            $em->persist($document);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_clients_index'));
        }

        return $this->render('AdminBundle:clients:upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a profile entity.
     *
     * @param Profile $profile
     *
     * @return Response
     */
    public function showAction(Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);

        return $this->render('AdminBundle:clients:show.html.twig', array(
            'profile'     => $profile,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $editForm   = $this->createForm('ApiBundle\Form\ProfileClientEditType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $profile->setUpdateDate(new DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('admin_clients_edit', array('id' => $profile->getId()));
        }

        return $this->render('AdminBundle:clients:formClients.html.twig', array(
            'profile'     => $profile,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'isEdit' => true,
        ));
    }

    /**
     * Deletes a profile entity.
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Profile $profile)
    {
        $form = $this->createDeleteForm($profile);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $em->remove($profile);
        $em->remove($profile->getPerson());
        $em->flush();
//        if ($form->isSubmitted() && $form->isValid()) {
//        }

        return $this->redirectToRoute('admin_clients_index');
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return Form The form
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_clients_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function showImportAction($role)
    {
        $importUsers = $this->getDoctrine()->getManager()->getRepository(ImportUser::class)->findBy([
            'importType' => $role,
        ]);

        return $this->render('AdminBundle:import:import.html.twig', ['importedUsers' => $importUsers]);
    }
}
