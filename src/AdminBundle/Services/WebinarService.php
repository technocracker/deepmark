<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24/08/18
 * Time: 09:36.
 */

namespace AdminBundle\Services;

class WebinarService
{
    private $accessSvc;
    private $baseUrl = 'api.getgo.com/G2W/rest/v2';

    public function __construct()
    {
        $this->accessSvc = new AccessService();
    }

    public function addAction()
    {
    }

    public function getAction()
    {
    }

    public function getsAction()
    {
    }

    public function removeAction()
    {
    }

    public function getUserInfo()
    {
        return $this->accessSvc->getAccountInfo();
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }
}
