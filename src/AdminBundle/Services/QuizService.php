<?php

namespace AdminBundle\Services;

use ApiBundle\Entity\Quizes;
use ApiBundle\Entity\QuizSteps;
use ApiBundle\Entity\QuizQuestions;
use ApiBundle\Entity\QuizQuestionsItems;
use ApiBundle\Entity\QuizQuestionsXref;
use Symfony\Component\DependencyInjection\ContainerInterface;

class QuizService {

    private $params;
    private $store;
    private $container;
    private $em;
    private $quiz_id;
    private $step_id;
    private $question_id;

    /**
     * Construct
     */
    public function __construct(ContainerInterface $container) {
        // Init
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->store = [];
    }

    /**
     * Save question
     * -> Save quiz if not saved
     * -> Save step if not saved
     * -> Save question
     * 
     * @param array quiz_id, step_id, question_id
     */
    public function saveQuestion(array $params) {
        $this->params = $params;
        // Create or get current
        $this->setStorage('quiz');
        $this->setStorage('step');
        $this->setStorage('question');
        $this->saveQuiz();
        $this->saveStep();
        $this->saveQuestionDatas();

        return array($this->quiz_id, $this->step_id, $this->question_id);
    }

    /**
     * Save Quiz
     */
    private function saveQuiz() {
        // Set datas
        if(strpos($this->params['quiz_id'], 'tmp') !== false) {
            $this->store['quiz']->setStatus(0);
        }
        $this->store['quiz']->setTitle($this->params['quiz']['title']);
        $this->store['quiz']->setDuration($this->params['quiz']['duration']);
        $this->store['quiz']->setSkills_or_and($this->params['quiz']['skills_or_and']);
        $this->store['quiz']->setCorrection_duration($this->params['quiz']['correction_duration']);
        $this->store['quiz']->setIsSurvey($this->params['quiz']['is_survey']);
        if(isset($this->params['quiz']['skills']) && is_array($this->params['quiz']['skills'])) {
            // Get previous skills & delete
            $currentSkills = $this->store['quiz']->getSkills();
            if($currentSkills) {
                foreach($currentSkills as $skill) {
                   $this->store['quiz']->removeSkill($skill); 
                }
            } 
            // Add skills
            foreach($this->params['quiz']['skills'] as $skill) {
                $this->store['quiz']->addSkill($this->container->get('doctrine')->getManager()->getRepository('ApiBundle:Skill')->findOneBy(['id' => $skill]));
            }
        }

        // Save
        $this->em->persist($this->store['quiz']);
        $this->em->flush();
        $this->quiz_id = $this->store['quiz']->getId();
    }

    /**
     * Save step
     */
    private function saveStep() {
        // Set datas
        $this->store['step']->setTitle($this->params['step']['title']);
        $this->store['step']->setOrdering($this->params['step']['ordering']);
        $this->store['step']->setStatus(1);
        $this->store['step']->setParams(json_encode($this->params['step']['params']));
        $this->em->persist($this->store['step']);
        $this->em->flush();

        // Save
        $this->store['quiz']->addStep($this->store['step']);
        $this->em->persist($this->store['quiz']);
        $this->em->flush();
        $this->step_id = $this->store['step']->getId();
    }

    /**
     * Save question datas
     */
    private function saveQuestionDatas() {
        $type = $this->params['question']['type'];
        // Set datas
        $this->store['question']->setType($type);
        $this->store['question']->setIsSurvey($this->params['question']['is_survey']);
        $this->store['question']->setQuestion($this->params['question']['question']);
        $this->store['question']->setScore_to_pass($this->params['question']['score_to_pass']);
        $this->store['question']->setStatus(1);
        $this->store['question']->setParams(json_encode($this->params['question']['params']));
        $this->store['question']->setIsLearnMore($this->params['question']['is_learn_more']);
        $this->em->persist($this->store['question']);
        $this->em->flush();

        // Copy files if import from history
        if (!empty($this->params['question']['history_id'])) {
            // Copy files
            $this->cloneFiles($this->params['question']['history_id'], $this->store['question']->getId(), json_decode($this->store['question']->getParams()));
        }

        // Question_id
        $this->question_id = $this->store['question']->getId();

        // Delete items if previously added for same question --> Set status = 0
        $em = $this->container->get('doctrine')->getManager();
        $items = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(QuizQuestionsItems::class)
                ->findBy(['question_id' => $this->store['question']->getId()]);
        //print $this->store['question']->getId() . ' : ';
        if ($items) {
            foreach ($items as $item) {
                //print $item->getId() . ' | ';
                $item->setStatus(0);
                $em->persist($item);
                $em->flush();
            }
        }

        // Add items
        $items = [];
        if (isset($this->params['question']['items'])) {
            foreach ($this->params['question']['items'] as $item) {
                // Create item
                $quizQuestionItem = new QuizQuestionsItems();
                // Global
                $quizQuestionItem->setType($item['type']);
                $quizQuestionItem->setScores(0);

                // Specific treatment for each question type
                switch ($item['type']) {
                    case 'qcu' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        break;
                    case 'qcm' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        break;
                    case 'drag_and_drop' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                        // Move first image
                        $this->moveFiles($item['value'], 'drag-and-drop');
                        $dragAndDropFiles = array($item['value']);
                        // Move match image if imageToImage
                        if ($item['specifics']['match_type'] == 'image') {
                            $this->moveFiles($item['specifics']['match'], 'drag-and-drop');
                            $dragAndDropFiles[] = $item['specifics']['match'];
                        }
                        // Move from history
                        if (!empty($this->params['question']['history_id'])) {
                            $this->cloneFiles($this->params['question']['history_id'], $this->question_id, null, $dragAndDropFiles);
                        }
                        break;
                    case 'ordering' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        break;
                    case 'category' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                        break;
                    case 'text_to_fill' :
                        $quizQuestionItem->setName($item['name']);
                        // Edit content before save, create an ampty boxe for each element : <span class="marker" data-pos="{position}"></span> 
                        if (isset($item['specifics']) && isset($item['value'])) {
                            foreach ($item['specifics']['options'] as $key => $specifics) {
                                $specifics = preg_replace('#\)#si', '&rpar;', preg_replace('#\(#si', '&lpar;', preg_replace('#\+#si', '&plus;', htmlentities($specifics))));
                                $item['value'] = preg_replace('#\)#si', '&rpar;', preg_replace('#\(#si', '&lpar;',  preg_replace('#\+#si', '&plus;', preg_replace("#&\#39;#si", "'", $item['value']))));
                                //$item['value'] = preg_replace('/<span class="marker">'.$specifics.'<\/span>/', '<span class="marker" data-pos="'.$key.'"></span>', $item['value']);
                                if (strpos($item['value'], '<span class="marker">' . $specifics . '</span>') !== false) {
                                    $item['value'] = preg_replace('#<span class="marker"[^>]*>' . $specifics . '</span>#si', '<span class="marker" data-pos="' . $key . '"></span>', $item['value']);
                                } else {
                                    unset($item['specifics']['options'][$key]);
                                }
                            }
                        }
                        
                        $item['specifics']['options'] = array_values($item['specifics']['options']);
                        
                        $quizQuestionItem->setValue($item['value']);
                        if (isset($item['specifics'])) {
                            $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                        }
                        break;
                    case 'open' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        if (isset($item['specifics'])) {
                            $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                            // Move template file
                            if(isset($item['specifics']['file'])) {
                                $template = $item['specifics']['file'];
                                if (!empty($template)) {
                                    $this->moveFiles($template, 'doc');
                                }
                            }
                        }
                        break;
                    
                    case 'default_correction' :
                        $quizQuestionItem->setType($item['type']);
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        if (isset($item['specifics'])) {
                            $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                            // Move files to real folder
                            if (!empty($item['specifics']['image'])) {
                                $this->moveFiles($item['specifics']['image'], 'picture');
                            }
                            if (!empty($item['specifics']['doc'])) {
                                $this->moveFiles($item['specifics']['doc'], 'doc');
                            }
                            if (!empty($item['specifics']['song'])) {
                                $this->moveFiles($item['specifics']['song'], 'song');
                            }
                            if (!empty($item['specifics']['video'])) {
                                $this->moveFiles($item['specifics']['video'], 'video');
                            }
                        }
                        break;
                    
                    case 'personalized_correction' :
                        $quizQuestionItem->setType($item['type']);
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        if (isset($item['specifics'])) {
                            $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                            // Move template file
                            if(isset($item['specifics']['file'])) {
                                $template = $item['specifics']['file'];
                                if (!empty($template)) {
                                    $this->moveFiles($template, 'doc');
                                }
                            }
                        }
                        break;
                    case 'scoring' :
                        $quizQuestionItem->setName($item['name']);
                        $quizQuestionItem->setValue($item['value']);
                        $quizQuestionItem->setSpecifics(json_encode($item['specifics']));
                        break;
                }
                $items[] = $quizQuestionItem;
            }
        }

        if ($items) {
            foreach ($items as $item) {
                $item->setStatus(1);
                $item->setQuestion($this->store['question']);
                $this->store['question']->setItem($item);
            }
        }

        $this->em->persist($this->store['question']);
        $this->em->flush();


        // Move files to real folder
        $image = $this->params['question']['params']['image'];
        $doc = $this->params['question']['params']['doc'];
        $song = $this->params['question']['params']['song'];
        $video = $this->params['question']['params']['video'];
        // Move good files to real folder
        $imageGood = $this->params['question']['params']['image_good'];
        $docGood = $this->params['question']['params']['doc_good'];
        $songGood = $this->params['question']['params']['song_good'];
        $videoGood = $this->params['question']['params']['video_good'];

        if (!empty($image)) {
            $this->moveFiles($image, 'picture');
        }
        if (!empty($doc)) {
            $this->moveFiles($doc, 'doc');
        }
        if (!empty($song)) {
            $this->moveFiles($song, 'song');
        }
        if (!empty($video)) {
            $this->moveFiles($video, 'video');
        }

        if (!empty($imageGood)) {
            $this->moveFiles($imageGood, 'picture');
        }
        if (!empty($docGood)) {
            $this->moveFiles($docGood, 'doc');
        }
        if (!empty($songGood)) {
            $this->moveFiles($songGood, 'song');
        }
        if (!empty($videoGood)) {
            $this->moveFiles($videoGood, 'video');
        }

        // Check xRef already added
        $foundXref = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(QuizQuestionsXref::class)
                ->findOneBy(['question' => $this->question_id]);
        if (!$foundXref) {
            // Add QuestionXref
            $questionXref = new QuizQuestionsXref();
            $questionXref->setQuiz($this->store['quiz']);
            $questionXref->setStep($this->store['step']);
            $questionXref->setQuestion($this->store['question']);
            $this->em->persist($questionXref);
            $this->em->flush();
            $this->store['quiz']->addQuestionXref($questionXref);
        }


        // Add to quiz
        $this->store['quiz']->addQuestion($this->store['question']);

        $this->em->persist($this->store['quiz']);
        $this->em->flush();
    }

    /**
     * Set Storage
     * 
     * @param string $type
     */
    private function setStorage($type) {
        // Init
        $foundRecord = false;

        // Switch type
        switch ($type) {
            case 'quiz' :
                $repository = Quizes::class;
                break;
            case 'step' :
                $repository = QuizSteps::class;
                break;
            case 'question' :
                $repository = QuizQuestions::class;
                break;
        }

        // Check if type exists (Quiz / Step / Question)
        if (isset($this->params[$type . '_id']) && strpos($this->params[$type . '_id'], 'tmp') === false) {
            $foundRecord = $this->container
                    ->get('doctrine')
                    ->getManager()
                    ->getRepository($repository)
                    ->findOneBy(['id' => $this->params[$type . '_id']]);
        }

        // Switch type
        if (!$foundRecord) {
            switch ($type) {
                case 'quiz' :
                    $entity = new Quizes();
                    break;
                case 'step' :
                    $entity = new QuizSteps();
                    break;
                case 'question' :
                    $entity = new QuizQuestions();
                    break;
            }
            // Store new type
            $this->store[$type] = $entity;
        } else {
            // Store existing type
            $this->store[$type] = $foundRecord;
        }
    }

    /**
     * Get Items
     * @param int $questionId
     */
    public function getItems($questionId) {
        return $this->container
                        ->get('doctrine')
                        ->getManager()
                        ->getRepository(QuizQuestionsItems::class)
                        ->findBy(['question_id' => $questionId, 'status' => 1]);
    }

    /**
     * Move file in front
     * 
     * @param string $file
     * @param string $type
     */
    public function moveFiles($file, $type) {
        // Base app path
        $base_path = $this->container->getParameter('kernel.root_dir') . '/../';
        // Real path previously saved
        $path = $base_path . 'web/files/InterventionFiles/Quiz/' . $this->params['quiz_id'] . '/' . $type . '/';
        // Specific case -> user create question > upload files > save questions (saved to tmp_id and save with real id)
        if (!file_exists($path . $file)) {
            $path = $base_path . 'web/files/InterventionFiles/Quiz/' . $this->params['quiz_tmp_id'] . '/' . $type . '/';
        }
        // New dir to save
        $newDir = $base_path . 'web/files/quiz/' . $this->question_id;
        // Check dir exists
        $this->checkDir($newDir);
        // Check dir/type existsd
        $newDir .= '/' . $type;
        $this->checkDir($newDir);
        // Move File in new dir
        if (file_exists($path . $file)) {
            rename($path . $file, $newDir . '/' . $file);
        }
    }

    /**
     * Check if dir exist, if not create
     * @param string $dir
     */
    private function checkDir($dir) {
        if (!is_dir($dir)) {
            mkdir($dir);
        }
    }

    /**
     * Update positions step / question
     * 
     * @param int $quiz_id step / question
     * @param array $steps
     * @param array $questions
     */
    public function updatePosition($quiz_id = null, $steps = null, $questions = null) {
        // Find Quiz
        $quiz = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository(Quizes::class)
                ->findOneBy(['id' => $quiz_id]);

        if ($quiz) {
            foreach ($quiz->getQuestions() as $question) {
                // Curr Step Id
                $curr_stepId = $question->getStep()->getId();
                // Search step in parameters
                $stepFound = array_search($curr_stepId, array_column($steps, 'id'));
                if ($stepFound !== false) {
                    // Change step position
                    $curr_stepParams = json_decode($question->getStep()->getParams());
                    $curr_stepParams->position = $steps[$stepFound]['position'];
                    $question->getStep()->setParams(json_encode($curr_stepParams));
                }

                // Curr Question Id
                $curr_questionId = $question->getQuestion()->getId();
                // Search question in parameters
                $questionFound = array_search($curr_questionId, array_column($questions, 'id'));
                if ($questionFound !== false) {
                    // Change step position
                    $curr_questionParams = json_decode($question->getQuestion()->getParams());
                    $curr_questionParams->position = $questions[$questionFound]['position'];
                    $question->getQuestion()->setParams(json_encode($curr_questionParams));
                }
            }

            // Save
            $this->em->persist($quiz);
            $this->em->flush();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Change status for question / step / quiz
     * 
     * @param string $type question / step / quiz
     * @param int $status
     * @param int $id
     */
    public function updateStatus($type = null, $status = null, $id = null) {
        switch ($type) {
            case 'quiz' :
                $repository = Quizes::class;
                break;
            case 'step' :
                $repository = QuizSteps::class;
                break;
            case 'question' :
                $repository = QuizQuestions::class;
                break;
        }

        // Find
        $foundRecord = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository($repository)
                ->findOneBy(['id' => $id]);

        // Change status
        if ($foundRecord) {
            $foundRecord->setStatus($status);
            $this->em->persist($foundRecord);
            $this->em->flush();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Duplicate Quiz
     * 
     * @param Quizes $quiz
     * @return int
     */
    public function duplicateQuiz(Quizes $quiz = null) {
        // Clone quiz
        $new_quiz = clone $quiz;
        $new_quiz->setCreated(new \DateTime('now'));
        $new_quiz->setTitle($quiz->getTitle().' #2');
        $new_quiz->setStatus(0);
        $this->em->persist($new_quiz);
        $this->em->flush();

        // Keep steps
        $allSteps = [];

        // Get all questions
        $questions = $quiz->getQuestions();
        if ($questions) {
            foreach ($questions as $question) {
                $question_id = $question->getQuestion()->getId();
                $curr_stepId = $question->getStep()->getId();
                $stepFound = array_search($curr_stepId, array_column($allSteps, 'id'));
                if ($stepFound === false) {
                    // Clone step
                    $new_step = clone $question->getStep();
                    $this->em->persist($new_step);
                    $this->em->flush();
                    $allSteps[] = ['id' => $curr_stepId, 'new_step' => $new_step];
                } else {
                    $new_step = $allSteps[$stepFound]['new_step'];
                }

                // Clone question
                $new_question = new QuizQuestions();
                // Set datas
                $new_question->setType($question->getQuestion()->getType());
                $new_question->setIsSurvey($question->getQuestion()->getIsSurvey());
                $new_question->setQuestion($question->getQuestion()->getQuestion());
                $new_question->setScore_to_pass($question->getQuestion()->getScore_to_pass());
                $new_question->setParams($question->getQuestion()->getParams());

                $this->em->persist($new_question);
                $this->em->flush();

                // Copy files
                $this->cloneFiles($question_id, $new_question->getId(), json_decode($question->getQuestion()->getParams()));

                // Clone items
                $items = $this->getItems($question_id);
                if ($items) {
                    foreach ($items as $item) {
                        // Clone question for new question
                        $new_question_item = new QuizQuestionsItems();
                        // Set datas
                        $new_question_item->setQuestion($new_question);
                        $new_question_item->setType($item->getType());
                        $new_question_item->setName($item->getName());
                        $new_question_item->setValue($item->getValue());
                        $new_question_item->setSpecifics($item->getSpecifics());
                        $new_question_item->setScores($item->getScores());
                        $new_question_item->setStatus(1);

                        $this->em->persist($new_question);
                        $this->em->flush();

                        $new_question->setItem($new_question_item);
                    }
                }
                $this->em->persist($new_question);
                $this->em->flush();

                // Create xRef
                $questionXref = new QuizQuestionsXref();
                $questionXref->setQuiz($new_quiz);
                $questionXref->setStep($new_step);
                $questionXref->setQuestion($new_question);
                $this->em->persist($questionXref);
                $this->em->flush();
                $new_quiz->addQuestionXref($questionXref);
            }
        }
        $this->em->persist($new_quiz);
        $this->em->flush();

        return $new_quiz->getId();
    }

    /**
     * Clone files (duplicate & import from history)
     * 
     * @param int $originQuestionId
     * @param int $destQuestionId
     * @param object $params
     * @param array $drag_and_drop [file1, file2, file_n]
     */
    private function cloneFiles($originQuestionId, $destQuestionId, $params = null, $drag_and_drop = null) {
        $basePathOrigin = $this->container->getParameter('kernel.root_dir') . '/../web/files/quiz/' . $originQuestionId . '/';
        $basePathDest = $this->container->getParameter('kernel.root_dir') . '/../web/files/quiz/' . $destQuestionId . '/';

        $this->checkDir($basePathDest);
        if ($params != null && !empty($params->image)) {
            $this->checkDir($basePathDest . '/picture/');
            @copy($basePathOrigin . 'picture/' . $params->image, $basePathDest . 'picture/' . $params->image);
        }
        if ($params != null && !empty($params->doc)) {
            $this->checkDir($basePathDest . '/doc/');
            @copy($basePathOrigin . 'doc/' . $params->doc, $basePathDest . 'doc/' . $params->doc);
        }
        if ($params != null && !empty($params->song)) {
            $this->checkDir($basePathDest . '/song/');
            @copy($basePathOrigin . 'song/' . $params->song, $basePathDest . 'song/' . $params->song);
        }
        if ($params != null && !empty($params->video)) {
            $this->checkDir($basePathDest . '/video/');
            @copy($basePathOrigin . 'video/' . $params->video, $basePathDest . 'video/' . $params->video);
        }
        if ($drag_and_drop != null) {
            $this->checkDir($basePathDest . '/drag-and-drop/');
            foreach ($drag_and_drop as $file) {
                @copy($basePathOrigin . 'drag-and-drop/' . $file, $basePathDest . 'drag-and-drop/' . $file);
            }
        }
    }

}
