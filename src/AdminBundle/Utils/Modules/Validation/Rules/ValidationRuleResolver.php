<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 19.03.19
 * Time: 14:34.
 */

namespace AdminBundle\Utils\Modules\Validation\Rules;

class ValidationRuleResolver implements RuleResolver
{
    private $validationRule;

    private $validators = [
        'integer' => IntegerRule::class,
        'string' => StringRule::class,
        'hourminuteduration' => HourMinuteDurationRule::class,
    ];

    private $value;

    public function setValidationRule(array $rule)
    {
        $this->validationRule = $rule;
    }

    public function getValidationRule(): ?array
    {
        return $this->validationRule;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getValidator(): ?RuleAbstract
    {
        if ('not_empty' != $this->validationRule[0]) {
            return null;
        }

        $this->validationRule = explode('|', ($this->validationRule[1]));
        $classNameToSearch = strtolower($this->validationRule[0]);

        if (!array_key_exists($classNameToSearch, $this->validators)) {
            return null;
        }

        /** @var RuleAbstract $validatorClass * */
        $validatorClass = new $this->validators[$classNameToSearch]();
        $validatorClass->setRuleResolver($this);

        return $validatorClass;
    }
}
