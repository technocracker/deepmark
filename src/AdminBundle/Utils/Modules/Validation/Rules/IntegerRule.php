<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 19.03.19
 * Time: 14:31.
 */

namespace AdminBundle\Utils\Modules\Validation\Rules;

class IntegerRule extends RuleAbstract
{
    const MESSAGE = 'Field value cannot be: ';

    public function validate(): RuleAbstract
    {
        $fieldValue = (int) $this->ruleResolver->getValue();

        $validationRules = $this->ruleResolver->getValidationRule();

        if (isset($validationRules[1])) {
            if ($fieldValue < 1) {
                $this->validationResult = self::MESSAGE.$fieldValue;
            }
        }

        return $this;
    }
}
