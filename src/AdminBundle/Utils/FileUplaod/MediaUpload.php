<?php
/**
 * Created by PhpStorm.
 * User: cyclad
 * Date: 15.02.19
 * Time: 14:43.
 */

namespace AdminBundle\Utils\FileUplaod;

use AdminBundle\Utils\Contracts\InterventionUpload;
use Symfony\Component\Filesystem\Filesystem;
use AdminBundle\Exception\IncorrectArgumentException;

class MediaUpload extends Filesystem
{
    private $request;

    private const UPLOADER_ID = 'media';

    private $dirToUpload;

    public function __construct(InterventionUpload $uploader)
    {
        $this->request = $uploader->getRequest();
        $this->dirToUpload = $uploader->getUploadFolder(self::UPLOADER_ID);
    }

    public function saveFile(): void
    {
        $uploadMaxSize = (int) ini_get("upload_max_filesize") * 1024 * 1024;
        if(!$this->request->files->all()) {
            throw new IncorrectArgumentException('File size exceeds the maximum limit of '.$uploadMaxSize.' MB');
        }

        foreach ($this->request->files as $file) {
            if (is_array($file)) {
                foreach ($file as $folderName => $fileArray) {
                    foreach ($fileArray['file'] as $personRole => $fileObject) {
                        if ($personRole == 'certificateTemplate'){
                            // allow only doc and docx
                            if(!in_array($fileObject->getMimeType(), [
                                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                'application/msword'
                            ])){
                                throw new \Exception('Only allow file types: *.doc, *.docx');
                            }
                        }
                        if($fileObject->getSize() > $uploadMaxSize) {
                            throw new IncorrectArgumentException('File size exceeds the maximum limit of '.$uploadMaxSize.' MB');
                        }
                        $filePath = $this->dirToUpload.'/'.$folderName.'/'.$personRole.'/'.$fileObject->getClientOriginalName();
                        $fileObject->move($this->dirToUpload.'/'.$folderName.'/'.$personRole, $fileObject->getClientOriginalName());
                    }
                }
            }
        }
    }
}
