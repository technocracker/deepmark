<?php

namespace DependencyInjection\Compiler;

use ApiBundle\Entity\Configurations;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ParametersCompilerPass
 * @package DependencyInjection\Compiler
 */
class ParametersCompilerPass implements CompilerPassInterface
{
    private $entityManager;

    private $default = [
        'list_items_limit' => 5,
    ];

    /**
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        $this->entityManager = $container->get('doctrine.orm.default_entity_manager');

        foreach ($this->default as $key => $value) {
            $container->setParameter('cfg_'.$key, $value);
        }

        $schemaManager = $this->entityManager->getConnection()->getSchemaManager();

        if (!$schemaManager->tablesExist(array('configurations'))) {
            return;
        }

        if (!$configurations = $this->entityManager->getRepository(Configurations::class)->findAll()) {
            return;
        }

        foreach ($configurations as $configuration) {
            $container->setParameter('cfg_'.$configuration->getName(), $configuration->getValue());
        }
    }
}
