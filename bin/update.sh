#!/bin/sh

echo "Switch back to develop branch"
git checkout -f develop
echo "Fetch latest code"
git fetch upstream
echo "Merging"
git merge upstream/develop
echo "Push origin"
git push origin develop

echo "Update latest vendor"
composer update

echo "Migrate database"
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:schema:update --force

echo "Reset database"
php bin/console doctrine:fixtures:load --no-interaction

echo "Clear cache"
php bin/console cache:clear --env=dev
php bin/console cache:clear --env=prod