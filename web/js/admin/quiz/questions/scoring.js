quizService.scoring = {
    active_section() {
        return ['question_msg'];
    },
    init(step_id, question_id) {
        quizService.qcu.addOption(step_id, question_id);
    },
    // Add an option process
    addOptionProcess(step_id, question_id, datas = null) {

    },
    // Add an option
    addOption(step_id, question_id) {

    },
    // Get items
    getItems(step_id, question_id) {
        let items = [];
        
        // Base input name
        const base_input = '#step-' + step_id + '-q-' + question_id + '-';
        
        // Free
        if ($(base_input+'free').is(':checked')) {
            items.push({
                type: 'scoring',
                name: 'free',
                value: $(base_input+ 'free_answer').val(),
                specifics: {
                    min: $(base_input+ 'free_min').val(),
                    max: $(base_input+ 'free_max').val(),
                    dec: $(base_input+ 'free_dec').val() ? $(base_input+ 'free_dec').val() : 0,
                    answer: $(base_input+ 'free_answer').val(),
                }
            });
        }
        
        // Stars
        if ($(base_input+'stars').is(':checked')) {
            items.push({
                type: 'scoring',
                name: 'stars',
                value: $(base_input+ 'stars_answer').val(),
                specifics: {
                    val: $(base_input+ 'stars_val').val(),
                    min: $(base_input+ 'stars_min').val(),
                    max: $(base_input+ 'stars_max').val(),
                    answer: $(base_input+ 'stars_answer').val(),
                }
            });
        }
        
        // Bubbles
        if ($(base_input+'bubbles').is(':checked')) {
            items.push({
                type: 'scoring',
                name: 'bubbles',
                value: $(base_input+ 'bubbles_answer').val(),
                specifics: {
                    val: $(base_input+ 'bubbles_val').val(),
                    min: $(base_input+ 'bubbles_min').val(),
                    max: $(base_input+ 'bubbles_max').val(),
                    answer: $(base_input+ 'bubbles_answer').val(),
                }
            });
        }
        
        // Slider
        if ($(base_input+'slider').is(':checked')) {
            items.push({
                type: 'scoring',
                name: 'slider',
                value: $(base_input+ 'slider_answer').val(),
                specifics: {
                    min: $(base_input+ 'slider_min').val(),
                    max: $(base_input+ 'slider_max').val(),
                    step: $(base_input+ 'slider_step').val(),
                    answer: $(base_input+ 'slider_answer').val(),
                }
            });
        }
        return items;
    },
    // Validate
    validate(step_name, question_name, items, step_id, question_id, isSurvey) {
        // Base input name
        const base_input = '#step-' + step_id + '-q-' + question_id + '-';
        
        
        // Free
        if ($(base_input+'free').is(':checked')) {
            const { min, max, dec, answer } = items[0].specifics;
            // Min
            if(!min)  {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_min};
            }
            // Max
            if(!max) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_max};
            }
            // Compare Min vs Max
            if(Number(min) > Number(max)) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_2};
            }
            // Dec
            if(dec && Number(dec) < 0) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_4};
                // return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_dec};
            }
            // Answer
            // if($('.typeQuestion.btn-info[data-question_id="step-' + step_id + '-q-' + question_id+'"]').data('type') == 'question') {
            if(!isSurvey) {
                // if($(base_input+'free_answer').val() == '' || isNaN($(base_input+'free_answer').val())) {
                if(!answer) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_answer};
                }
                // Check answer with min & max
                if(Number(answer) < Number(min) || Number(answer) > Number(max)) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_3};
                }
                // Check answer decimal
                const ansDecimal = answer.split(".")[1];
                
                if(!ansDecimal && Number(dec) != 0) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_3};
                }
                
                if(ansDecimal && ansDecimal.length != Number(dec)) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_3};
                }
            }
        }
        
        // Stars 
        if ($(base_input+'stars').is(':checked')) {
            // Val
            if(isNaN($(base_input+'stars_val').val())) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1};
            }
            // Answer
            if($('.typeQuestion.btn-info[data-question_id="step-' + step_id + '-q-' + question_id+'"]').data('type') == 'question') {
                if($(base_input+'stars_answer').val() == '' || isNaN($(base_input+'stars_answer').val())) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_answer};
                }
            }
        }
        
        // Bubbles 
        if ($(base_input+'bubbles').is(':checked')) {
            // Val
            if(isNaN($(base_input+'bubbles_val').val())) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1};
            }
            // Answer
            if($('.typeQuestion.btn-info[data-question_id="step-' + step_id + '-q-' + question_id+'"]').data('type') == 'question') {
                if($(base_input+'bubbles_answer').val() == '' || isNaN($(base_input+'bubbles_answer').val())) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_answer};
                }
            }
        }
        
        // Slider 
        if ($(base_input+'slider').is(':checked')) {
            // Min
            if($(base_input+'slider_min').val() == '' || isNaN($(base_input+'slider_min').val())) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_min};
            }
            // Max
            if($(base_input+'slider_max').val() == '' || isNaN($(base_input+'slider_max').val())) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_max};
            }
            // Step
            if(isNaN($(base_input+'slider_step').val())) {
                return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_step};
            }
            // Answer
            if($('.typeQuestion.btn-info[data-question_id="step-' + step_id + '-q-' + question_id+'"]').data('type') == 'question') {
                if($(base_input+'slider_answer').val() == '' || isNaN($(base_input+'slider_answer').val())) {
                    return {status: 'error', error: '<b>' + step_name + ' > ' + question_name + ' : </b><br />' + translations.error_scoring_1+' '+translations.error_scoring_answer};
                }
            }
        }
        
        // Success if no error
        return {status: 'success'}
    },
    // Set items
    setItems(step_id, question_id, datas) {
        if (datas) {
            const base_input = '#step-' + step_id + '-q-' + question_id + '-';
            
            datas.map((item, i) => {
                switch(item.name) {
                    case 'free' :
                        $(base_input+'free').attr('checked', 'checked');
                        $(base_input+'free_min').val(item.specifics.min);
                        $(base_input+'free_max').val(item.specifics.max);
                        $(base_input+'free_dec').val(item.specifics.dec);
                        $(base_input+'free_answer').val(item.specifics.answer);
                        break;
                    case 'stars' :
                        $(base_input+'stars').attr('checked', 'checked');
                        $(base_input+'stars_val').val(item.specifics.val);
                        $(base_input+'stars_min').val(item.specifics.min);
                        $(base_input+'stars_max').val(item.specifics.max);
                        $(base_input+'stars_answer').val(item.specifics.answer);
                        break;
                    case 'bubbles' :
                        $(base_input+'bubbles').attr('checked', 'checked');
                        $(base_input+'bubbles_val').val(item.specifics.val);
                        $(base_input+'bubbles_min').val(item.specifics.min);
                        $(base_input+'bubbles_max').val(item.specifics.max);
                        $(base_input+'bubbles_answer').val(item.specifics.answer);
                        break;
                    case 'slider' :
                        $(base_input+'slider').attr('checked', 'checked');
                        $(base_input+'slider_min').val(item.specifics.min);
                        $(base_input+'slider_max').val(item.specifics.max);
                        $(base_input+'slider_step').val(item.specifics.step);
                        $(base_input+'slider_answer').val(item.specifics.answer);
                        break;
                }
            });
        }
    }
}