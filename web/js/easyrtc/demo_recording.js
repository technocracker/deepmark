var selfEasyrtcid = "";
var supportsRecording = easyrtc.supportsRecording();

function connect(person) {
    if( !supportsRecording) {
       window.alert("This browser does not support recording. Try chrome or firefox.");
    } else {
      document.getElementById("recording").disabled = false;
      if( easyrtc.isRecordingTypeSupported("h264")) document.getElementById("useH264").disabled = false;
      if( easyrtc.isRecordingTypeSupported("vp9")) document.getElementById("useVP9").disabled = false;
      if( easyrtc.isRecordingTypeSupported("vp8")) document.getElementById("useVP8").disabled = false; 
    }

    easyrtc.setSocketUrl('https://172.20.0.16:8443');
    easyrtc.setVideoDims(640,480);
    // move error alert to console.log at https://groups.google.com/forum/#!msg/easyrtc/tilf5ym84Tg/Bm2GFaEADwAJ
    easyrtc.setOnError( function(errEvent) { console.log('Alert error: ' + errEvent.errorText);});

    //leaveJoinedRoom(document.getElementById("individual_room").value);

    if (person == 'trainer')
        easyrtc.easyApp("easyrtc.audioVideoSimple", "selfVideo", ["callerVideo"], trainerLoginSuccess, loginFailure);
    else
        easyrtc.easyApp("easyrtc.audioVideoSimple", "selfVideo", ["callerVideo"], learnerLoginSuccess, loginFailure);

 }

function joinRoomSuccess(roomName) {
   console.log("join room success" + roomName);
   for (var room in easyrtc.getRoomsJoined()) {
       if (room == 'default') {
           easyrtc.leaveRoom('default', leaveRoomSuccess, leaveRoomFailed);
       }
   }
}

function joinRoomFailed() {
    console.log("join room failed");
}

function leaveRoomSuccess(roomName) {
    console.log("leave room success room name: "+ roomName);
}

function leaveRoomFailed(errorCode, errorText, roomName) {
    console.log("leave room failed " + errorCode + ' Error text: ' + errorText + ' room name: '+ roomName);
}

function clearConnectList() {
    var otherClientDiv = document.getElementById('otherClients');
    while (otherClientDiv.hasChildNodes()) {
        otherClientDiv.removeChild(otherClientDiv.lastChild);
    }
}


function convertListToButtons (roomName, data, isPrimary) {
    clearConnectList();
    var otherClientDiv = document.getElementById('otherClients');
    for(var easyrtcid in data) {
        console.log(roomName);
        console.log(easyrtcid);

        // Fixed the "Cannot set remote offer in state have-local-offer" error message
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function() {
            //your code to be executed after 1 second
        }, delayInMilliseconds);

        performCall(easyrtcid);

        var button = document.createElement('button');
        button.className = 'btn btn-info';
        button.onclick = function(easyrtcid) {
            return function() {
                performCall(easyrtcid);
            };
        }(easyrtcid);

        // var label = document.createTextNode(easyrtc.idToName(easyrtcid));
        var label = document.createTextNode('Rejoin');
        button.appendChild(label);
        otherClientDiv.appendChild(button);
    }
}


function performCall(otherEasyrtcid) {
    easyrtc.hangupAll();
    var successCB = function() { };
    var failureCB = function() {};
    easyrtc.call(otherEasyrtcid, successCB, failureCB);
}


function trainerLoginSuccess(easyrtcid) {
    console.log('trainer Login Success');
    selfEasyrtcid = easyrtcid;
    document.getElementById("iam").innerHTML = "I am " + easyrtc.cleanId(easyrtcid);
    let roomName = document.getElementById("individual_room").value;
    easyrtc.joinRoom(roomName, null, joinRoomSuccess, joinRoomFailed);
    easyrtc.setRoomOccupantListener(convertListToButtons);
    easyrtc.getRoomList(showRoomList, null);
}

function showRoomList(roomList){
    console.log('show Room List');
    for (var roomName in roomList) {
        console.log(roomList[roomName].numberClients + ' - ' + roomName);
    }
}

function learnerLoginSuccess(easyrtcid) {
    console.log('learner Login Success');
    selfEasyrtcid = easyrtcid;
    document.getElementById("iam").innerHTML = "I am " + easyrtc.cleanId(easyrtcid);
    let roomName = document.getElementById("individual_room").value;
    easyrtc.joinRoom(roomName, null, joinRoomSuccess, joinRoomFailed);
    // easyrtc.leaveRoom('default', null);
    easyrtc.setRoomOccupantListener(convertListToButtons);
    easyrtc.getRoomList(showRoomList, null);
}


function loginFailure(errorCode, message) {
    easyrtc.showError(errorCode, message);
}


var selfRecorder = null;
var callerRecorder = null;

function startRecording() {
    var selfLink = document.getElementById("selfDownloadLink");
    selfLink.innerText = "";

    selfRecorder = easyrtc.recordToFile( easyrtc.getLocalStream(),
               selfLink, "selfVideo");
    if( selfRecorder ) {
       document.getElementById("recording").innerHTML = "Stop recording";
    }
    else {
       window.alert("failed to start recorder for self");
       return;
    }

    var callerLink = document.getElementById("callerDownloadLink");
    callerLink.innerText = "";

    if( easyrtc.getIthCaller(0)) {
       callerRecorder = easyrtc.recordToFile(
           easyrtc.getRemoteStream(easyrtc.getIthCaller(0), null),
             callerLink, "callerVideo");
       if( !callerRecorder ) {
          window.alert("failed to start recorder for caller");
       }
    }
    else {
       callerRecorder = null;
    }
}


function endRecording() {
    if( selfRecorder ) {
       selfRecorder.stop();
    }
    if( callerRecorder ) {
       callerRecorder.stop();
    }
    document.getElementById("recording").innerHTML = "Start recording";
}
