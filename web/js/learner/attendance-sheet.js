$(document).ready(function() {
    bindAttendance();
});

function bindAttendance() {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''
    $('#attendance-sheet').on('show.bs.modal', function (e) {
        //e.preventDefault();
        $.ajax({
            'url': lang + '/learner/courses/get-attendance-content',
            'method': 'POST',
            'data': {
                'id': $('#bookingId').val(),
                'absent': 0,
            },
            beforeSend: function() {
                $("#content-attendance-sheet").empty().html('<i class="fa fa-spinner fa-spin"></i>');
            },
        }).done(function (response) {
            $("#content-attendance-sheet").html(response);
            $($(e.relatedTarget).data('target')).modal('show');

            $('.check-all-input').change(function() {
                if ($(this).is(':checked')) {
                    $(this).closest('.table').find('.form-check-input').prop('checked', true);
                } else {
                    $(this).closest('.table').find('.form-check-input').prop('checked', false);
                }
            });
        }).fail(function (response) {
            swal(
                "Error", jsTranslations.attendance_error, "error",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        });
    });

    // Draw image
    let signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        penColor: 'rgb(0, 0, 0)'
    });
    let saveButton = document.getElementById('save');
    let cancelButton = document.getElementById('clear');

    saveButton.addEventListener('click', function (event) {
        console.log(signaturePad.isEmpty())
        if (!signaturePad.isEmpty()) {
            let signature = signaturePad.toDataURL('image/png');
            let bookingId = $('#signature-image').find('input[name="bookId"]').val();
            let signatureType = $('#signature-image').find('input[name="signatureType"]').val();
            let signatureName = $('#signature-image').find('input[name="signatureName"]').val();

            // Send data to server instead...
            $.ajax({
                'url': '/learner/courses/save-signature',
                'method': 'POST',
                'data': {
                    'signature': signature,
                    'bookingId': bookingId,
                    'signatureType': signatureType,
                }
            }).done(function (response) {
                if (response.disabledButton) {
                    $('.signature-button-' + bookingId).attr('disabled', 'disabled');
                }

                swal({
                    type: 'info',
                    allowOutsideClick: false,
                    title: jsTranslations.save_signature_title,
                    text: jsTranslations.save_signature_text,
                }).then(
                    function () {
                        $('#signature-image').modal('hide');
                        //loadAttendanceSheet(lang)
                    },
                )
            }).fail(function (response) {
                swal(
                    "Error", jsTranslations.attendance_error, "error",
                    {
                        showCancelButton: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }
                );
            });
        } else {
            swal(
                "Warning", jsTranslations.please_sign_text, "warning",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        }
        // window.open(data);
    });

    cancelButton.addEventListener('click', function (event) {
        signaturePad.clear();
    });

    // Keep booking ID and signatureType at the signature-image modal
    /*$('#signature-image').on('show.bs.modal', function(e) {
        let bookId = $(e.relatedTarget).attr('signature-data');
        $(e.currentTarget).find('input[name="bookId"]').val(bookId);

        let signatureType = $(e.relatedTarget).attr('signature-type');
        let signatureName = $(e.relatedTarget).attr('signature-name');
        $(e.currentTarget).find('input[name="signatureType"]').val(signatureType);
        $(e.currentTarget).find('input[name="signatureName"]').val(signatureName);
        $(e.currentTarget).find('span[id="signatureNumber"]').text(signatureName);
        $("#clear").trigger("click");
    });*/
}

function bindAllowAttendanceSheet() {
    let language = $('#locate').val();
    let default_locale = $('#default_locale').val();
    let lang = language != default_locale ? '/' + language : ''
    $('.closeOpenSignature').on('click', function(event) {
        event.preventDefault();
        const signatures = [];
        let typeSignature = $('#typeSignature').val();
        let id = $(this).attr("data-id");
        let title = ''
        let text = ''
        if (id == 1) {
            title = jsTranslations.open_signature_title;
            text = jsTranslations.open_signature_text;
        } else {
            title = jsTranslations.close_signature_title;
            text = jsTranslations.close_signature_text;
        }
        $('.signature-input').each(function(){
            if($(this).is(':checked')){
                signatures.push({
                    bookingId: $(this).val(),
                    allowSignature: id,
                });
            }
        });

        $.ajax({
            'url': '/trainer/exercices/allow-signatures',
            'method': 'POST',
            'data': {
                'signatures': signatures,
                'typeSignature': typeSignature,
            }
        }).done(function (response) {
            swal({
                type: 'info',
                allowOutsideClick: false,
                title: title,
                text: text,
            }).then(
                function () {
                    $('#attend-absent').modal('hide');
                    loadAttendanceSheet(lang)
                },
            )
        }).fail(function (response) {
            swal(
                "Error", jsTranslations.attendance_error, "error",
                {
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                }
            );
        });
    });
}

function loadAttendanceSheet(lang) {
    $.ajax({
        'url': lang + '/learner/courses/get-attendance-content',
        'method': 'POST',
        'data': {
            'id': $('#bookingId').val(),
            'absent': 0,
        },
        beforeSend: function() {
            $("#content-attendance-sheet").empty().html('<i class="fa fa-spinner fa-spin"></i>');
        },
    }).done(function (response) {
        $("#content-attendance-sheet").html(response);
        $('#attendance-sheet').modal('show');

        $('.check-all-input').change(function() {
            if ($(this).is(':checked')) {
                $(this).closest('.table').find('.form-check-input').prop('checked', true);
            } else {
                $(this).closest('.table').find('.form-check-input').prop('checked', false);
            }
        });
    }).fail(function (response) {
        swal(
            "Error", jsTranslations.attendance_error, "error",
            {
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true
            }
        );
    });
}