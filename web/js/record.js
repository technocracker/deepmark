/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

// This code is adapted from
// https://rawgit.com/Miguelao/demos/master/mediarecorder.html

'use strict';

/* globals MediaRecorder */

let mediaRecorder = [];
let recordedBlobs = [];

const recordElements = document.querySelectorAll('.record');
recordElements.forEach(element => {
    element.addEventListener('click', async (event) => {
        var targetElement = event.target || event.srcElement;
        let type = targetElement.getAttribute('data-type');

        let constraints = {
            audio: true,
            video: {
                width: 1280, height: 720
            }
        };
        if(type == 'audio'){
            constraints = {
                audio: true
            };
        }

        console.log('Using media constraints:', constraints);
        await init(constraints, type);

        const recordButton = document.querySelector('#record-'+type);
        const playButton = document.querySelector('#play-'+type);
        const downloadButton = document.querySelector('#download-'+type);
        if (recordButton.getAttribute('data-record') === '1') {
            startRecording(type);
        } else {
            stopRecording(type);
            recordButton.innerHTML = '<i class="fa fa-circle font-size-11 disabled"></i> '+jsTranslations.record;
            recordButton.setAttribute('data-record', "1");
            playButton.disabled = false;
            downloadButton.disabled = false;
        }
    });
});

const playElements = document.querySelectorAll('.play');
playElements.forEach(element => {
    element.addEventListener('click', (event) => {
        var targetElement = event.target || event.srcElement;
        let type = targetElement.getAttribute('data-type');
        const recordedVideo = document.querySelector('#recorded-'+ type);
        const superBuffer = new Blob(recordedBlobs[type], {type: type+'/webm'});
        recordedVideo.src = null;
        recordedVideo.srcObject = null;
        recordedVideo.src = window.URL.createObjectURL(superBuffer);
        recordedVideo.controls = true;
        recordedVideo.play();
    });
});

const downloadElements = document.querySelectorAll('.download');
downloadElements.forEach(element => {
    element.addEventListener('click', () => {
        var targetElement = event.target || event.srcElement;
        let type = targetElement.getAttribute('data-type');
        const blob = new Blob(recordedBlobs[type], {type: type+'/webm'});
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        a.download = type + (new Date()).getTime() + '.webm';
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 100);
    });
});

function startRecordingAudio() {
    $('#recording-audio i').fadeOut(900).delay(300).fadeIn(800)
}
function startRecording(type) {
    if (type == 'audio') {
        setInterval(startRecordingAudio,1500);
        $('#recording-'+type).show();
        $('#recording-'+type+' i').fadeOut(900).delay(300).fadeIn(800);
    }
    recordedBlobs[type] = [];
    const recordButton = document.querySelector('#record-'+type);
    const playButton = document.querySelector('#play-'+type);
    const downloadButton = document.querySelector('#download-'+type);
    let options = {mimeType: 'video/webm;codecs=vp9,opus'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.error(`${options.mimeType} is not supported`);
        options = {mimeType: 'video/webm;codecs=vp8,opus'};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.error(`${options.mimeType} is not supported`);
            options = {mimeType: 'video/webm'};
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
                console.error(`${options.mimeType} is not supported`);
                options = {mimeType: ''};
            }
        }
    }
    if (type === 'audio'){
        options = {mimeType: 'audio/webm'};
    }

    try {
        mediaRecorder[type] = new MediaRecorder(window.stream, options);
        const recordedVideo = document.querySelector('#recorded-'+ type);
        recordedVideo.setAttribute('src', "");
    } catch (e) {
        console.error('Exception while creating MediaRecorder:', e);
        let errorMsgElement = document.querySelector('#errorMsg-'+type);
        errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(e)}`;
        return;
    }

    console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
    recordButton.textContent = $('#stop_text').val();
    recordButton.setAttribute('data-record', "0");
    playButton.disabled = true;
    downloadButton.disabled = true;
    mediaRecorder[type].onstop = (event) => {
        console.log('Recorder stopped: ', event);
        console.log('Recorded Blobs: ', recordedBlobs);
    };
    mediaRecorder[type].ondataavailable = e => recordedBlobs[type].push(e.data);;
    mediaRecorder[type].start();
    console.log('MediaRecorder started', mediaRecorder[type]);
}

function stopRecording(type) {
    if (type == 'audio') {
        $('#recording-'+type).hide();
    }
    mediaRecorder[type].stop();
    $(`#deleteContent_${type}`).prop( "disabled", false );
}

function handleSuccess(stream, type) {
    const recordButton = document.querySelector('#record-'+type);
    recordButton.disabled = false;
    console.log('getUserMedia() got stream:', stream);
    window.stream = stream;

    const gumVideo = document.querySelector('video#gum-'+type);
    gumVideo.srcObject = stream;
}

function removeBlobs(type){
    delete recordedBlobs[type];
}

async function init(constraints, type) {
    try {
        const stream = await navigator.mediaDevices.getUserMedia(constraints);
        handleSuccess(stream, type);
    } catch (e) {
        console.error('navigator.getUserMedia error:', e);
        let errorMsgElement = document.querySelector('#errorMsg-'+type);
        errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
    }
}
const startElements = document.querySelectorAll('.start');
startElements.forEach(element => {
    element.addEventListener('click', async (event) => {
        // const hasEchoCancellation = document.querySelector('#echoCancellation').checked;
        var targetElement = event.target || event.srcElement;
        const type = targetElement.getAttribute('data-type');
        let constraints = {
            audio: true,
            video: {
                width: 1280, height: 720
            }
        };
        if(type == 'audio'){
            constraints = {
                audio: true
            };
        }

        console.log('Using media constraints:', constraints);
        await init(constraints, type);
    });
});

const delContentVideo = document.querySelector('button#deleteContent-video');

// let fileUpload = [];

// function getFilesUpload(element) {
//     fileUpload = element.files;
// }

$(document).on('click', '.create-best-practice', function () {
    let id = $(this).attr('id');

    let fd = new FormData();
    let existResource = false;
    const emptyContent = [];

    if($('#divContent > .block-audio').length) {
        existResource = true;
        if (recordedBlobs['audio']){
            const audioBlob = new Blob(recordedBlobs['audio'], {type: 'audio/webm'});
            fd.append('audioBestPractice', audioBlob);
        } else {
            emptyContent.push('audio');
        }
    }

    if($('#divContent > .block-video').length) {
        existResource = true;
        if (recordedBlobs['video']){
            const videoBlob = new Blob(recordedBlobs['video'], {type: 'video/webm'});
            fd.append('videoBestPractice', videoBlob);
            existResource = true;
        } else {
            emptyContent.push('video');
        }
    }

    const fileUpload = $('[name="fileUpload"]').prop('files');

    if (fileUpload){
        existResource = true;
        if(fileUpload.length > 0){
            fd.append('fileUploadBestPractice', fileUpload[0]);
        } else {
            emptyContent.push('file');
        }
    }

    if($('[name="url"]').length){
        existResource = true;
        if($('[name="url"]').val().length > 0) {
            if($('[name="url"]').attr('data-validation') == 1) {
                fd.append('urlBestPractice',$('[name="url"]').val());
            } else {
                swal('', $('#invalid_url').val(), 'warning');
                return false;
            }
        } else {
            emptyContent.push('url');
        }
    }

    if($('[name="textBestPractice"]').length){
        existResource = true;
        if($('[name="textBestPractice"]').val().length > 0) {
            fd.append('textBestPractice', $('[name="textBestPractice"]').val());
        } else {
            emptyContent.push('textarea');
        }
    }
    // validation data here
    if(existResource == false){
        swal('', $('#required_content_text').val(), 'warning');
        return false;
    }

    // validation data here
    if($('[name=title]').val() == "" || $('[name=description]').val() == "" || $('[name="tags"]').val() == ""){
        swal('', $('#input_required_text').val(), 'warning');
        return false;
    }

    if(emptyContent.length > 0) {
        swal('', `${emptyContent.join(', ')} are empty`, 'warning');
        return false;
    }

    //end validation
    fd.append('titleBestPractice', $('[name=title]').val());
    fd.append('descriptionBestPractice', $('[name=description]').val());
    fd.append('tagsBestPractice', $('[name="tags"]').val());
    fd.append('orderBestPractice', $('.sortable').sortable("toArray"));
    fd.append('draft', id == 'saveDraftBestPractice' ? 1 : 0);

    $.ajax({
        type: "POST",
        url: $('#url_add_extra').val(),
        data: fd,
        processData: false,
        contentType: false,
    }).done(function (data) {
        if (id == 'saveDraftBestPractice') {
            window.open(
                data.url,
                '_blank' // <- This is what makes it open in a new window.
            );
        } else {
            swal('', data.message, 'success');
            setTimeout(function () {
                window.location = $('#url_best_practices').val();
            }, 1500)
        }
    });
});